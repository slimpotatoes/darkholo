# Darkholo software : Process darkfield holograms to get 2D deformation field and 2D strain tensor

# --------------------------------------------
# GUI // I/O libraries
# --------------------------------------------

# GUI Tkinter
import Tkinter
# Open file dialog
import tkFileDialog

# Need to open a tk-window else Mac crash
window_DFH = Tkinter.Tk()
# Hide the window
window_DFH.withdraw()

# Matplotlib libraries for GUI
import matplotlib
import matplotlib.lines
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.widgets import Button
from matplotlib.widgets import RadioButtons
# DM3 reader
import dm3_lib

# --------------------------------------------
# Processing libraries
# --------------------------------------------

# Mathematical library for 2D array processing
import numpy

# --------------------------------------------
# Darkholo processing functions
# --------------------------------------------


class darkholo:
    def __init__(self):
        # Initialization of variables ---------
        # Images to be processed
        self.image1 = ""
        self.image2 = []
        # Size in pixel of the image
        self.height_image = 0
        self.width_image = 0
        # Crystal type
        self.crystal = ""
        # Crystal lattice
        self.a = 0.
        # Reciprocal vectors g1,g2 used
        self.g1 = 0
        # darkholo.g2 = 0
        # FFT of the parent image
        self.image_fft = []
        # Circle used to mask the FFT
        self.circle = plt.Circle(0, 0, color='b', fill=False)
        # Radius of the circul
        self.radius = 0.0
        # Center circle position
        self.position = [0, 0]
        # FFT image masked
        self.image_fft_masked = ""
        # Phase image of the masked FFT
        self.phase1 = ""
        self.phase2 = ""
        # Amplitude image of the masked FFT
        self.amplitude1 = ""
        # self.amplitude2 = ""
        # Position of 2 points of the g vector
        self.gpoint = [0, 0]
        # Line of the g vector
        self.lineg = ""
        # Norm of g vector in Fourier space
        self.gx = 0
        self.gy = 0
        # Derivative of the phase image along x and y directions
        self.phase_derivative0 = ""
        self.phase_derivative1 = ""

    # Open a file function
    def openfile(self):
        filepath = tkFileDialog.askopenfilename(title="Open an image")
        self.image1 = dm3_lib.DM3(filepath)
        self.image2 = self.image1.imagedata
        self.height_image = self.image1.height
        self.width_image = self.image1.width
        print "Image loaded"
        print self.image1.info
        print "pixel size = %s %s" % darkholo.image1.pxsize
        print self.height_image, " x ", self.width_image, " pixels"
        # Other information of the image to add

    # Perform the (Fast) Fourier Transform of the hologram and show the power spectrum shifted
    def fft(self):
        self.image_fft = numpy.fft.fftshift(numpy.abs(numpy.fft.fft2(self.image2))**2)
        print("FFT performed")

    # Create a mask on the circles area to isolate the interesting frequency and generate phase + amplitude images
    def mask(self):
        fft_mask = numpy.zeros((self.width_image, self.height_image))
        correction = numpy.zeros((self.width_image, self.height_image))
        for i in range(0, self.width_image):
            for j in range(0, self.height_image):
                if ((i - self.position[1]) ** 2 + (j - self.position[0]) ** 2) < self.radius ** 2:
                    fft_mask[i][j] = 1
        for k in range(0, self.width_image):
            for l in range(0, self.height_image):
                correction[k][l] = 2 * numpy.pi * (self.gx * k + self.gy * l)
        self.image_fft_masked = numpy.multiply(fft_mask, numpy.fft.fftshift(numpy.fft.fft2(self.image2)))
        self.amplitude1 = numpy.absolute(numpy.fft.ifft2((numpy.fft.ifftshift(self.image_fft_masked))))
        self.phase1 = numpy.angle(numpy.fft.ifft2((numpy.fft.ifftshift(self.image_fft_masked))))
        self.phase2 = self.phase1-correction-(numpy.round((self.phase1-correction)/2/numpy.pi))*2*numpy.pi

    # Find the maximum of the circle and define norme of gx and gy
    def max_circle(self):
        k = 0
        l = 0
        for i in range(0, self.width_image):
            for j in range(0, self.height_image):
                if (((i - self.position[1]) ** 2 + (j - self.position[0]) ** 2) < (self.radius ** 2)) and \
                                self.image_fft[i][j] > self.image_fft[k][l]:
                    k = i
                    l = j
        self.position = [l, k]
        self.gx = (self.position[1] - 0.5 * self.width_image) / self.width_image
        self.gy = (self.position[0] - 0.5 * self.height_image) / self.height_image


    # Derivative of the phase
    def derivative(self):
        derive_temp = numpy.gradient(numpy.exp(self.phase2 * 1j))
        print derive_temp
        print numpy.shape(derive_temp)
        self.phase_derivative0 = numpy.imag(numpy.multiply(numpy.exp(-self.phase2 * 1j), derive_temp[0]))
        self.phase_derivative1 = numpy.imag(numpy.multiply(numpy.exp(-self.phase2 * 1j), derive_temp[1]))
        print numpy.shape(self.phase_derivative0)


# --------------------------------------------
# GUI
# --------------------------------------------

class GUI():
    def __init__(self):
        self.selected_figure = 1

    def start(self,darkholo):
        self.darkholo = darkholo

        # Layout in matplotlib using GridSpec
        fig1 = plt.figure(1)
        self.fig1ax1 = fig1.add_subplot(2,2,1)
        self.fig1ax1.annotate('DFH image', xy=(.5, 1.1), xycoords='axes fraction', horizontalalignment='center',
                          verticalalignment='top', fontsize=12)
        self.fig1ax2 = fig1.add_subplot(2,2,3)
        self.fig1ax2.annotate('FFT', xy=(.5, 1.1), xycoords='axes fraction', horizontalalignment='center',
                          verticalalignment='top', fontsize=12)
        self.fig1ax7 = fig1.add_subplot(2,2,2)
        self.fig1ax7.annotate('Amplitude', xy=(.5, 1.1), xycoords='axes fraction', horizontalalignment='center',
                          verticalalignment='top', fontsize=12)
        self.fig1ax8 = fig1.add_subplot(2,2,4)
        self.fig1ax8.annotate('Phase', xy=(.5, 1.1), xycoords='axes fraction', horizontalalignment='center',
                          verticalalignment='top', fontsize=12)

        self.fig1ax1.axis('on')
        self.fig1ax2.axis('on')
        # ax6.axis('on')

        #button1 = Button(self.ax3, 'Open')
        #button1.on_clicked(self.openfile)
        #button2 = Button(self.ax4, 'FFT')
        #button2.on_clicked(self.fft)
        #button3 = Button(self.ax5, 'Draw g')
        #button3.on_clicked(self.drawg)
        #button4 = Button(self.ax6, 'Mask')
        #button4.on_clicked(self.mask)

        # Layout in matplotlib using GridSpec
        fig2 = plt.figure(2)
        self.fig2ax1 = fig2.add_subplot(2, 2, 1)
        self.fig2ax1.annotate('DFH image', xy=(.5, 1.1), xycoords='axes fraction', horizontalalignment='center',
                          verticalalignment='top', fontsize=12)
        self.fig2ax2 = fig2.add_subplot(2, 2, 3)
        self.fig2ax2.annotate('FFT', xy=(.5, 1.1), xycoords='axes fraction', horizontalalignment='center',
                          verticalalignment='top', fontsize=12)
        self.fig2ax7 = fig2.add_subplot(2, 2, 2)
        self.fig2ax7.annotate('Amplitude', xy=(.5, 1.1), xycoords='axes fraction', horizontalalignment='center',
                          verticalalignment='top', fontsize=12)
        self.fig2ax8 = fig2.add_subplot(2, 2, 4)
        self.fig2ax8.annotate('Phase', xy=(.5, 1.1), xycoords='axes fraction', horizontalalignment='center',
                          verticalalignment='top', fontsize=12)

        # Another figure with buttons
        figbutton = plt.figure(3)
        self.buttonax1 = figbutton.add_subplot(5,1,1)
        self.buttonax2 = figbutton.add_subplot(5,1,2)
        self.buttonax3 = figbutton.add_subplot(5,1,3)
        self.buttonax4 = figbutton.add_subplot(5,1,4)
        self.buttonax5 = figbutton.add_subplot(5,1,5)

        figbutton_button1 = Button(self.buttonax1, 'Open')
        figbutton_button1.on_clicked(self.openfile)
        figbutton_button2 = Button(self.buttonax2, 'FFT')
        figbutton_button2.on_clicked(self.fft)
        figbutton_button3 = Button(self.buttonax3, 'Mask')
        figbutton_button3.on_clicked(self.mask)
        figbutton_button4 = Button(self.buttonax4, 'Draw g')
        figbutton_button4.on_clicked(self.drawg)
        figbutton_button5 = RadioButtons(self.buttonax5, ('Figure 1', 'Figure 2'))
        figbutton_button5.on_clicked(self.choose_figure)


        fig1.canvas.mpl_connect('button_press_event', self.change_circle)
        fig1.canvas.mpl_connect('key_press_event', self.change_posrad)
        fig1.canvas.mpl_connect('key_press_event', self.defgpoint)

        fig2.canvas.mpl_connect('button_press_event', self.change_circle)
        fig2.canvas.mpl_connect('key_press_event', self.change_posrad)
        fig2.canvas.mpl_connect('key_press_event', self.defgpoint)

        fig1.canvas.mpl_connect('close_event', GUI.closeprogram)

        #plt.draw()
        plt.show()
        # window_DFH.mainloop()

    def openfile(self, event):
        self.darkholo.openfile()
        plt.figure(self.selected_figure) # selected figure
        if self.selected_figure == 1:
            self.fig1ax1.imshow(darkholo.image1.imagedata, cmap="gray")
        elif self.selected_figure == 2:
            self.fig2ax1.imshow(darkholo.image1.imagedata, cmap="gray")
            print "hej"
        plt.draw()

    def fft(self, event):
        self.darkholo.fft()
        plt.figure(self.selected_figure) # activate selected figure
        if self.selected_figure ==1:
            # Show the image in log scale (very large intensity)
            self.fig1ax2.imshow(numpy.log1p(darkholo.image_fft), cmap="gray")
            # Draw the first circle on the middle of the FFT
            self.radius = (darkholo.height_image) / 10.0
            self.position = (darkholo.width_image/ 2,darkholo.height_image/ 2)
            darkholo.circle = plt.Circle(self.position, self.radius, color='b', fill=False)
            self.fig1ax2.add_artist(darkholo.circle)
            plt.draw()
        elif self.selected_figure ==2:
            # Show the image in log scale (very large intensity)
            self.fig2ax2.imshow(numpy.log1p(darkholo.image_fft), cmap="gray")
            # Draw the first circle on the middle of the FFT
            self.radius = (darkholo.height_image) / 10.0
            self.position = (darkholo.width_image / 2, darkholo.height_image / 2)
            darkholo.circle = plt.Circle(self.position, self.radius, color='b', fill=False)
            self.fig2ax2.add_artist(darkholo.circle)
            plt.draw()

    # Define the new position and the new radius of the circle to mask the FFT
    def change_posrad(self, event):
        if event.key == 'a' and self.fig1ax2 == event.inaxes or self.fig1ax2 == event.inaxes :
            darkholo.position = (event.xdata, event.ydata)
            print "position=", darkholo.position
        if event.key == 'z' and self.fig1ax2 == event.inaxes:
            darkholo.radius = numpy.sqrt((event.xdata - darkholo.position[0]) ** 2 + (event.ydata - darkholo.position[1]) ** 2)
            print("r=", darkholo.radius)

    # Draw a circle in the new position with the new radius
    def change_circle(self, event):
        if event.button == 1 and self.fig1ax2 == event.inaxes:
            darkholo.circle.remove()
            darkholo.max_circle()
            print darkholo.position, darkholo.radius
            darkholo.circle = plt.Circle(darkholo.position, darkholo.radius, color='b', fill=False)
            self.fig1ax2.add_artist(darkholo.circle)
            event.canvas.draw()
            print("Circle drawn")

    # Draw the masked FFT
    def mask(self, event):
        darkholo.mask()
        plt.figure(self.selected_figure)
        self.fig1ax2.imshow(numpy.log1p(numpy.abs(darkholo.image_fft_masked)), cmap="gray")
        print "masked"
        self.fig1ax7.imshow(numpy.log10(darkholo.amplitude1), cmap="gray")
        self.fig1ax8.imshow(darkholo.phase2, cmap="gray")
        plt.draw()
        fig2 = plt.figure()
        ax = plt.gca()
        ax.imshow(darkholo.phase2, cmap="gray")
        fig2.show()
        plt.figure(self.selected_figure)

    # TO BE COMMENTED WHEN NEEDED
    def defgpoint(self, event):
        if event.key == 'a' and self.fig1ax1 == event.inaxes:
            darkholo.gpoint[0] = (event.xdata, event.ydata)
            print darkholo.gpoint
        if event.key == 'z' and self.fig1ax1 == event.inaxes:
            darkholo.gpoint[1] = (event.xdata, event.ydata)
            print darkholo.gpoint

    def drawg(self, event):
        plt.figure(self.selected_figure)
        self.lineg = matplotlib.lines.Line2D([darkholo.gpoint[0][0], darkholo.gpoint[1][0]],
                                             [darkholo.gpoint[0][1], darkholo.gpoint[1][1]], color='r')
        self.fig1ax1.add_artist(self.lineg)
        darkholo.derivative()
        fig4 = plt.figure()
        axxx = fig4.add_subplot(121)
        axxx.imshow(darkholo.phase_derivative0, cmap="bwr", vmin=-0.1, vmax=0.1)
        axxxx = fig4.add_subplot(122)
        axxxx.imshow(darkholo.phase_derivative1, cmap="bwr", vmin=-0.1, vmax=0.1)
        fig4.show()
        plt.figure(self.selected_figure)
        print 'g line drawn'

    # Figure 1 or Figure 2
    def choose_figure(self, label):
        figure_dict = {'Figure 1': 1, 'Figure 2': 2}
        self.selected_figure = figure_dict[label]
        plt.figure(self.selected_figure)
        print self.selected_figure


    def closeprogram(event):
        print "Closing pgm"
        plt.close('all')
        window_DFH.destroy()
        window_DFH.quit()


darkholo = darkholo()
myGUI = GUI()
myGUI.start(darkholo)
window_DFH.mainloop()