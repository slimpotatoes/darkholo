# Darkholo2

# --------------------------------------------
# GUI // I/O libraries
# --------------------------------------------

# GUI Tkinter
import Tkinter
# Open file dialog
import tkFileDialog

# Need to open a tk-window else Mac crash
window_DFH = Tkinter.Tk()
# Hide the window
window_DFH.withdraw()

# Matplotlib libraries for GUI
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.lines as lines
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import matplotlib.pyplot as plt
import matplotlib.patches as patch
import matplotlib.gridspec as gridspec
from matplotlib.widgets import Button
from matplotlib.widgets import RadioButtons

# DM3 reader
import dm3_lib

# --------------------------------------------
# Processing libraries
# --------------------------------------------

# Mathematical library for 2D array processing
import numpy
from scipy.optimize import leastsq
from skimage.restoration import unwrap_phase


# PENSER A ENLEVER LES VARIABLES GLOBALES,
# 1 Reorganiser en 3 parties : UI, Proc, script
# 2 designer les fonction (entree sortie)
# 3 simplifier les actions
# 4 Data storage (a penser)

# --------------------------------------------
# Controler Darkholo
# --------------------------------------------

class darkholo:
    def __init__(self):
        # Initialization of variables ---------
        self.init=0

    # Open the image and store the image (function of the figure)
    # Input, self + filepath + the selected figure
    # Output the image.imagedate to be able to show it
    #  !!! Add a way to deal with non .dm3 file !!!
    def openimage(self, filepath, selected_figure):
        image1 = dm3_lib.DM3(filepath)
        #self.height_image = self.image1.height
        #self.width_image = self.image1.width
        print image1
        print "Image loaded"
        print image1.info
        print "pixel size = %s %s" % image1.pxsize
        storage.saveimage(image1, selected_figure)
        return image1.imagedata

    # Perform the (Fast) Fourier Transform of the hologram and show the power spectrum shifted accordingly
    # Load image from the good figure
    # Save the fft of the image
    # Return the fft_image ready to be displayed
    def fft(self,selected_figure):
        image = storage.loadimage(selected_figure)
        image_fft = numpy.fft.fftshift(numpy.abs(numpy.fft.fft2(image.imagedata))**2)
        #print("FFT performed")
        storage.saveimagefft(image_fft,selected_figure)
        return (image_fft,image.width,image.height) # return the tuple image, width and height

    # Create a mask on the circles area to isolate the interesting frequency and generate phase + amplitude images
    # Load image, position of the circle center (q), radius
    # Create mask "0/1" 0 outside of the circle, 1 inside and multiply it with the fft to make fft masked
    # Probably possible to do it with numpy slicing
    # Generate "correction matrix" corresponding the the reference (0 phase) making the assumption that the majority
    # of the same orientation and frequency represents the reference
    # Perform the back FT and separate in amplitude + phase images (complex image)
    # Correct the phase by putting the reference matrix calculated above
    # Return, the fft masked (to see the masking result), amplitude and phase corrected
    def mask(self):
        selected_figure = myGUI.getcircle()[2]
        image = storage.loadimage(selected_figure)
        q = storage.loadq(selected_figure)
        #q = [0.2,0.2]
        position = myGUI.getcircle()[0]
        radius = myGUI.getcircle()[1]
        fft_mask = numpy.zeros((image.width, image.height))
        correction = numpy.zeros((image.width, image.height))
        temp_ones = numpy.ones((image.width, image.height))
        for i in range(0, image.width):
            for j in range(0, image.height):
                # Circular mask 0/1
                #if ((i - position[1]) ** 2 + (j - position[0]) ** 2) < (radius ** 2):
                    #fft_mask[i][j] = 1
            # 2D Gaussian mask to try reducing noise
                fft_mask[i][j] = 1 / ((numpy.sqrt(2 * numpy.pi)) * radius/2) * numpy.exp(
                    -((j - position[0]) ** 2 + (i - position[1]) ** 2) / (2 * (radius/2) ** 2))
        for k in range(0, image.width):
            for l in range(0, image.height):
                correction[k][l] = 2 * numpy.pi * (q[0] * k + q[1] * l)
        print " qx, qy =", q
        image_fft_masked = numpy.multiply(fft_mask, numpy.fft.fftshift(numpy.fft.fft2(image.imagedata)))
        amplitude1 = numpy.absolute(numpy.fft.ifft2((numpy.fft.ifftshift(image_fft_masked))))
        phase1 = numpy.angle(numpy.fft.ifft2((numpy.fft.ifftshift(image_fft_masked))))
        phase3 = unwrap_phase(phase1)
        test_corr = numpy.subtract(numpy.mod(numpy.add(correction,numpy.pi*temp_ones),2*numpy.pi*temp_ones),numpy.pi*temp_ones)
        phase4 = numpy.subtract(phase3,correction)
        phase5 = numpy.subtract(numpy.mod(numpy.add(phase4,numpy.pi*temp_ones),2*numpy.pi*temp_ones),numpy.pi*temp_ones)

        #correction1 = correction #- numpy.round((correction)/(2*numpy.pi))*2*numpy.pi
        #phase2 = phase1-correction-(numpy.round((phase1-correction)/(2*numpy.pi)))*2*numpy.pi
        storage.savephase_uncorr(phase1,selected_figure)
        storage.savephase_corr(phase5,selected_figure)
        return (image_fft_masked, amplitude1, phase5, phase3, phase1, correction)

    # Find the maximum of the circle and define norm of qx and qy (reference)
    # Input position, radius from GUI and selected_figure
    # Load image and image_fft
    # Correct the position of the circle on the maximum
    # Calculate and save the new position of g.
    def max_circle(self,position,radius,selected_figure):
        image = storage.loadimage(selected_figure)
        image_fft = storage.loadimagefft(selected_figure)
        k = 0
        l = 0
        for i in range(0, image.width):
            for j in range(0, image.height):
                if (((i - position[1]) ** 2 + (j - position[0]) ** 2) < (radius ** 2)) and \
                                image_fft[i][j] > image_fft[k][l]:
                    k = i
                    l = j
        # Event (x,y) coordinate are inverted compared to (x,y) array coordinate
        position_corr = [l, k]
        # Define the norm of the q vector for the correction
        # --- Need to be checked ------
        qx = (position_corr[1] - 0.5 * image.width) / image.width
        qy = (position_corr[0] - 0.5 * image.height) / image.height
        q = [qx, qy]
        storage.saveq(q,selected_figure)
        return position_corr

    # Function to define a reference in the phase image and correct the phase image
    # (put 0 as the phase in the reference area)
    # Function looks complicated because a 2D linear regression is performed to find the new qx,qy couple defining
    # the new reference
    # Output phase_corrected array that can be show like an image
    def reference(self,selected_figure):
        # Load the elements need
        image = storage.loadimage(selected_figure)
        phase_uncorr = storage.loadphase_uncorr(selected_figure)
        q = storage.loadq(selected_figure)
        # Take the position of the reference
        position_reference = myGUI.getreference()[0]
        pos_ref = numpy.round(position_reference).astype(int)
        #print pos_ref

        # Extract the information and generate the list/matrix to perform the least square root
        # temp_phase is the portion of phase extracted which will be the reference
        temp_phase = phase_uncorr[pos_ref[0][1]:pos_ref[1][1], pos_ref[0][0]:pos_ref[1][0]]
        # temp_x and temp_y represent the position matrix
        temp_x = numpy.ones((pos_ref[1][1]-pos_ref[0][1], pos_ref[1][0]-pos_ref[0][0]))*range(pos_ref[0][0],pos_ref[1][0])
        temp_y = numpy.transpose(numpy.ones((pos_ref[1][0]-pos_ref[0][0],pos_ref[1][1]-pos_ref[0][1]))*range(pos_ref[0][1],pos_ref[1][1]))

        temp_phase_uw = unwrap_phase(temp_phase)

        print "shape temp_phase, X, Y ", numpy.shape(temp_phase), numpy.shape(temp_x), numpy.shape(temp_y)
        print "temp_phase ", temp_phase
        print "temp_x ", temp_x
        print "temp_y ", temp_y
        temp_phase_reshape = numpy.reshape(temp_phase_uw,(1,numpy.size(temp_phase)))[0]
        temp_x_reshape = numpy.reshape(temp_x, (1, numpy.size(temp_x)))[0]
        temp_y_reshape = numpy.reshape(temp_y, (1, numpy.size(temp_y)))[0]
        print "shape temp_phase, X, Y ", numpy.shape(temp_phase_reshape)[0], numpy.shape(temp_x_reshape)[0], numpy.shape(temp_y_reshape)[0]


        # Minimize square root method to calculate => Phase_corr = q.r = q0*i + q1*j
        def residuals(p, y, x1, x2):
            q0, q1 = p
            # err = y - 2*numpy.pi*(q0 * x1 + q1 * x2)-numpy.round((y - 2*numpy.pi*(q0 * x1 + q1 * x2))/(2*numpy.pi))*2*numpy.pi
            err = y - 2*numpy.pi*(q1 * x1 + q0 * x2)
            err1 = numpy.gradient(err)
            return err1

        p0 = [q[0],q[1]]
        # Function doing them mathematical process
        plsq = leastsq(residuals, p0, args=(temp_phase_reshape, temp_x_reshape, temp_y_reshape),ftol=1.49012e-30,xtol=1.49012e-15)
        # New q corrected
        q_corr = plsq[0]
        print q, q_corr


        # Generate the correction matrix
        correction = numpy.zeros((image.width, image.height))
        for k in range(0, image.width):
            for l in range(0, image.height):
                correction[k][l] = 2 * numpy.pi * (q_corr[0] * k + q_corr[1] * l)
        # correct the phase
        phase_corr = phase_uncorr - correction - (numpy.round((phase_uncorr - correction) / (2 * numpy.pi))) * 2 * numpy.pi
        storage.savephase_corr(phase_corr, selected_figure)
        storage.saveq(q_corr,selected_figure)
        return phase_corr

    # Derivative of the phase
    # Input - figure on which the processing is done
    # Load phase image, derive it and save both derivative
    # In between some "not necessary rotations" are performed
    # Output - derivative images ready to be imaged
    def derivative(self,selected_figure):
        phase = storage.loadphase_corr(selected_figure)
        # Trick to avoid phase unwrap
        # For two dimensional arrays, the return will be two arrays ordered by axis. In this example the first array
        # stands for the gradient in rows and the second one in columns direction:
        derive_temp = numpy.gradient(numpy.exp(phase * 1j))
        # Derivative of the exponential to get this info -- grad(phase) = 2pi*(g_reg-g_roi) in the image base (x,y)
        phase_derivativex = -numpy.imag(numpy.multiply(numpy.exp(-phase * 1j), derive_temp[0]))
        phase_derivativey = -numpy.imag(numpy.multiply(numpy.exp(-phase * 1j), derive_temp[1]))
        # Rotate derivative phase image in the (X,Y) calibrated coordinate system (where eXX,eYY,eXY,rXY matters)
        theta_cal = self.rotate(selected_figure)[1]
        phase_derivativeX = numpy.cos(theta_cal)*phase_derivativex + numpy.sin(theta_cal)*phase_derivativey
        phase_derivativeY = -numpy.sin(theta_cal)*phase_derivativex + numpy.cos(theta_cal)*phase_derivativey
        phase_derivative = [phase_derivativex, phase_derivativey]
        storage.savederivephase(phase_derivative,selected_figure)
        # Rotate derivate phase image to be perp and para to g acquired (careful -- doubt on the formula of the rotation)
        theta = self.rotate(selected_figure)[0]
        # Calibrate g_ref in the x,y base and in pixel-1
        self.calibrate_g_ref(theta,selected_figure)
        phase_derivativeu = numpy.cos(theta)*phase_derivativex + numpy.sin(theta)*phase_derivativey
        phase_derivativev = -numpy.sin(theta)*phase_derivativex + numpy.cos(theta)*phase_derivativey
        return (phase_derivativeX, phase_derivativeY,phase_derivativeu,phase_derivativev)

    # Save the coordinate of the g line drawn on the figure, the information of the g recorded and the lattice
    # parameter of the crystal
    # Output calibrated g_ref in pixel to be drawn on darkholo image (GUI used)
    def gline(self,lineg,infog,a,selected_figure):
        gpoint = lineg
        ginfo = infog
        alattice = a
        storage.savelineg(gpoint,ginfo,alattice,selected_figure)
        theta = self.rotate(selected_figure)[0]
        g_ref = self.calibrate_g_ref(theta,selected_figure)
        return g_ref


    # Rotate the axes to be parallel and perpendicular to the DFH g acquired
    # Input - the vector g drawn, crystallographic planes
    # Output - rotation angle from image (x,y) to calibration (u,v)
    # Output - rotation angle from image (x,y) to g acquired
    def rotate(self,selected_figure):
        gpoint = storage.loadlineg(selected_figure)[0]
        ginfo = storage.loadlineg(selected_figure)[1]
        #lattice = storage.loadlineg(selected_figure)[2]
        print "Darkfield hologram g (hkl): ", ginfo[1]
        print "g drawn (hkl): ", ginfo[0]
        print "zone axis (hkl): ", ginfo[2]
        print "3rd axis (hkl): ", ginfo[3]

        # rotation from image to g drawn
        #print "tan(theta) =  ",(gpoint[1][0] - gpoint[0][0]) / (gpoint[1][1] - gpoint[0][1])
        #theta_g_drawn = numpy.arctan((gpoint[1][0] - gpoint[0][0]) / (gpoint[1][1] - gpoint[0][1]))

        g_drawn_proj_cos = (gpoint[1][1]-gpoint[0][1]) / \
                           (numpy.sqrt((gpoint[1][1] - gpoint[0][1])**2+(gpoint[1][0] - gpoint[0][0])**2))
        print "g_drawn_proj_cos : ",g_drawn_proj_cos
        # DOT PRODUCT ON G PERP
        g_drawn_proj_sin = (gpoint[1][0]-gpoint[0][0]) / \
                           (numpy.sqrt((gpoint[1][1] - gpoint[0][1])**2+(gpoint[1][0] - gpoint[0][0])**2))
        print "g_drawn_proj_sin : ", g_drawn_proj_sin

        if g_drawn_proj_sin >= 0:
            theta_g_drawn = numpy.arccos(g_drawn_proj_cos)
        else:
            theta_g_drawn = -numpy.arccos(g_drawn_proj_cos)

        # rotation from g drawn to g measured
        # Get the angle with the Dot product --- check that it's a planar vector (IMPORTANT)
        norm_g_drawn = numpy.sqrt(ginfo[0][0]**2+ginfo[0][1]**2+ginfo[0][2]**2)
        norm_g_acquired = numpy.sqrt(ginfo[1][0]**2+ginfo[1][1]**2+ginfo[1][2]**2)
        norm_g_3rdaxis = numpy.sqrt(ginfo[3][0]**2+ginfo[3][1]**2+ginfo[3][2]**2)
        print "norm = ",norm_g_drawn, norm_g_acquired, norm_g_3rdaxis

        # DOT PRODUCT ON G PARA
        proj_cos = numpy.dot(ginfo[1],ginfo[0])/(norm_g_acquired*norm_g_drawn)
        print "g_acq.gref = ",numpy.dot(ginfo[1],ginfo[0])
        print "proj_cos = ", proj_cos
        # DOT PRODUCT ON G PERP
        proj_sin = numpy.dot(ginfo[1],ginfo[3])/(norm_g_acquired*norm_g_3rdaxis)
        print "g_acq.grefperp = ",numpy.dot(ginfo[1], ginfo[3])
        print "proj_sin = ", proj_sin

        if proj_sin>=0:
            theta_g_aquired = numpy.arccos(proj_cos)
        else:
            theta_g_aquired = -numpy.arccos(proj_cos)

        # Total rotation angle (first rotation to X,Y axis and then to u,v)
        theta_rot = theta_g_drawn + theta_g_aquired
        print "theta_g_drawn = ", theta_g_drawn
        print "theta_g_aquired = ", theta_g_aquired
        print "theta_rot = ", theta_rot
        #print "norm g drawn: ", norm_g_drawn, " norm g aquired: ", norm_g_acquired, " norm g axis: ", norm_g_zaxis
        return theta_rot,theta_g_drawn

    # Calibrate g_ref in the x,y base in pixel
    # Input - image, pixel size, user inputs (crystallographic plane), lattice parameter,
    # Need to check the normalization of gref
    # Output g_refx and g_refy normalized in pixel, adapted coordinate of the g ref vector
    def calibrate_g_ref(self,theta,selected_figure):
        pixelsize = 0.0
        image = storage.loadimage(selected_figure)
        a = storage.loadlineg(selected_figure)[2]
        pix = image.pxsize
        size = image.height
        if pix[1] == 'micron':
            pixelsize = float(pix[0]*10000)
        else:
            pixelsize = float(pix[0]*10)

        #print "pixel size = ", pixelsize
        #print type(pixelsize)
        # (hkl) of the g acquired ==> g ref
        g_ref_info = storage.loadlineg(selected_figure)[1][1]
        print g_ref_info
        # Not sure about the normalization ..... !!!!!!!!! To be CHECKED !!!!!!!!!!
        g_ref_norm = pixelsize*numpy.sqrt((g_ref_info[0]**2+g_ref_info[1]**2+g_ref_info[2]**2)/a**2)
        print "g_ref_norm = ", g_ref_norm
        g_ref_x = g_ref_norm*numpy.cos(theta)
        g_ref_y = g_ref_norm*numpy.sin(theta)
        g_ref = [g_ref_x,g_ref_y]
        print "g_ref = ", g_ref
        g_ref_image = [size/2, size/2, g_ref_y*size/5, g_ref_x*size/5]
        #print "g_reg image = ", g_ref_image
        storage.save_g_vector_ref(g_ref,selected_figure)
        return g_ref,g_ref_image

    # Tentative of a way to calcualte the strain tensor ---- NOT USED HERE
    def strain_tensor2(self):
        phase_derivative_fig1 = storage.loadderivephase(1)
        phase_derivative_fig2 = storage.loadderivephase(2)
        #theta_cal = self.rotate(selected_figure)
        grad_u = numpy.array([[phase_derivative_fig1[0], phase_derivative_fig1[1]],
                              [phase_derivative_fig2[0], phase_derivative_fig2[1]]])
        # Need to load each g to generate matrix a ...
        a = numpy.array([[1.0, 0.0],[0.0, 1.0]]) # a multiplier par - 1/(2*numpy.pi)*
        print a
        print 'a[1,1] = ', a[1, 1]
        print 'a[1,0] = ', a[1, 0]
        print 'a[0,1] = ', a[0, 1]
        print 'a[0,0] = ', a[1, 1]

        # e = numpy.dot(a,grad_u)
        # e = numpy.array([a[0][0]*grad_u[0][0]+a[][]*grad_u[][],])
        # e = numpy.array([[a[0][0]*grad_u[0][0]+a[0][1]*grad_u[1][0],a[0][0]*grad_u[0][1]+a[0][1]*grad_u[1][1]],
        #                 [a[1][0]*grad_u[0][0]+a[1][1]*grad_u[1][0],a[1][0]*grad_u[0][1]+a[1][1]*grad_u[1][1]]])
        e = numpy.array(
            [[numpy.add(a[0][0]*grad_u[0][0],a[0][1]*grad_u[1][0]), numpy.add(a[0][0]*grad_u[0][1],a[0][1]*grad_u[1][1])],
             [numpy.add(a[1][0]*grad_u[0][0],a[1][1]*grad_u[1][0]), numpy.add(a[1][0]*grad_u[0][1],a[1][1]*grad_u[1][1])]])

        #print e[0][0]
        #print numpy.shape(e)
        #print numpy.shape(e[0][0])
        # epsilon = 1/2*(numpy.add(e,numpy.transpose(e)))
        epsilon = numpy.array([[e[0][0],0.5*(numpy.add(e[1][0],e[0][1]))],[0.5*(numpy.add(e[0][1],e[1][0])),e[1][1]]])
        # rotation = 1/2*(numpy.subtract(e,numpy.transpose(e)))
        rotation = numpy.array([[0.5*(numpy.subtract(e[0][0],e[0][0])),0.5*(numpy.subtract(e[0][1],e[1][0]))],
                                [0.5*(numpy.subtract(e[1][0],e[0][1])),0.5*(numpy.subtract(e[1][1],e[1][1]))]])
        return (epsilon,rotation)

    # Srain tensor calculation
    # Input - phase_derivative (x,y) from figure 1 and figure 2
    # Output - 2D maps of the strain tensor
    def strain_tensor(self):
        phase_derivative_fig1 = storage.loadderivephase(1)
        phase_derivative_fig2 = storage.loadderivephase(2)

        # Array 2x2x512x512 ... each quadrant has an image corresponding to phi1_x, phi1_y, phi2_x, phy2_y
        delta_g = 1/(2*numpy.pi)*numpy.array([[phase_derivative_fig1[0], phase_derivative_fig1[1]],
                              [phase_derivative_fig2[0], phase_derivative_fig2[1]]])

        g_ref_1 = storage.load_g_vector_ref(1)
        g_ref_2 = storage.load_g_vector_ref(2)
        id = numpy.identity(2)

        # Array 2x2x512x512 where each quadrant has one component of g_ref
        g_ref = numpy.array([[numpy.ones(numpy.shape(phase_derivative_fig1[0]))*g_ref_1[0],
                              numpy.ones(numpy.shape(phase_derivative_fig1[1]))*g_ref_1[1]],
                            [numpy.ones(numpy.shape(phase_derivative_fig2[0]))*g_ref_2[0],
                             numpy.ones(numpy.shape(phase_derivative_fig1[1]))*g_ref_2[1]]])

        # g_measured = g_ref - g_roi /// g_measured = delta g
        g_roi = numpy.subtract(g_ref,delta_g)
        # Array 512x512x2x2 each pixel has a g matrix = [[g1x,g2x],[g1y,g2y]]
        g_roi_pixel = numpy.transpose(g_roi,axes=[2,3,1,0])
        g_ref_pixel = numpy.transpose(g_ref,axes=[2,3,1,0])

        # Transpose g2x and g1y to create transpose of G
        t_g_roi_pixel = numpy.transpose(g_roi_pixel, axes=[0,1,3,2])
        t_g_ref_pixel = numpy.transpose(g_ref_pixel, axes=[0,1,3,2])

        # Inverse both matrix on each pixel
        t_g_roi_pixel_inv = numpy.linalg.inv(t_g_roi_pixel)

        # Generate distortion matrix
        d = numpy.zeros(numpy.shape(g_ref_pixel))
        for i in range(0,numpy.shape(phase_derivative_fig1[0])[0]):
            for j in range(0,numpy.shape(phase_derivative_fig1[0])[0]):
                d[i][j] = numpy.dot(t_g_roi_pixel_inv[i][j],t_g_ref_pixel[i][j])-id

        # Generate epsilon/rotation (512x512x2x2)
        epsilon_pixel = 0.5*numpy.add(d,numpy.transpose(d,axes=[0,1,3,2]))
        rotation_pixel = 0.5*numpy.subtract(d,numpy.transpose(d,axes=[0,1,3,2]))

        # Rotate strain tensor in (X,Y) base --- along g drawn and perp to g drawn (g calibrated)
        epsilon_pixel_rot = numpy.zeros(numpy.shape(epsilon_pixel))
        rotation_pixel_rot = numpy.zeros(numpy.shape(rotation_pixel))

        theta_to_g_drawn=self.rotate(1)[1]
        R = numpy.array([[numpy.cos(theta_to_g_drawn),-numpy.sin(theta_to_g_drawn)],[numpy.sin(theta_to_g_drawn),numpy.cos(theta_to_g_drawn)]])

        for i in range(0,numpy.shape(phase_derivative_fig1[0])[0]):
            for j in range(0,numpy.shape(phase_derivative_fig1[0])[0]):
                epsilon_pixel_rot[i][j] = numpy.dot(numpy.transpose(R),numpy.dot(epsilon_pixel[i][j],R))
                rotation_pixel_rot[i][j] = numpy.dot(numpy.transpose(R),numpy.dot(rotation_pixel[i][j],R))
        # Generate epsilon/rotation images (2x2x512x512)
        epsilon_image = numpy.transpose(epsilon_pixel_rot,axes=[2,3,0,1])
        rotation_image = numpy.transpose(rotation_pixel_rot,axes=[2,3,0,1])
        return (epsilon_image,rotation_image)

# --------------------------------------------
# Storage
# --------------------------------------------
class storage:
    def __init__(self):
        self.fig1_info = ''
        self.fig2_info = ''
        self.fig1_image = ''
        self.fig2_image = ''
        self.fig1_image_fft = ''
        self.fig2_image_fft = ''
        self.fig1_q = [0, 0]
        self.fig2_q = [0, 0]
        self.fig1_phase_uncorr = ''
        self.fig2_phase_uncorr = ''
        self.fig1_phase_corr = ''
        self.fig2_phase_corr = ''
        self.fig1_derive_phase = ''
        self.fig2_derive_phase = ''
        self.gpoint_fig1 = [(0,0),(0,0)]
        self.gpoint_fig2 = [(0,0),(0,0)]
        # ginfo (hkl): 0 - draw g ; 1 - acquired g ; 2 - zone axis - 3 third axis
        self.ginfofig1 = [[0,0,0],[0,0,0],[0,0,0],[0,0,0]]
        self.ginfofig2 = [[0,0,0],[0,0,0],[0,0,0],[0,0,0]]
        self.a_lattice = 0
        self.g_ref_vector_fig1 = [0,0]
        self.g_ref_vector_fig2 = [0,0]

    def saveinfo(self, info, selected_figure):
        if selected_figure ==1:
            self.fig1_info = info
            #print "Saved info from figure 1"
        elif selected_figure ==2:
            self.fig2_info = info
            #print "Saved info from figure 2"
        else:
            print "Saving process failed"

    def loadinfo(self, selected_figure):
        if selected_figure==1:
            #print "Loading info from figure 1"
            return self.fig1_info
        elif selected_figure==2:
            #print "Loading info from figure 2"
            return self.fig2_info
        else:
            print "Loading process failed"

    def saveimage(self, info, selected_figure):
        if selected_figure == 1:
            self.fig1_image = info
            #print "Saved image from figure 1"
        elif selected_figure == 2:
            self.fig2_image = info
            #print "Saved image from figure 2"
        else:
            print "Saving process failed"

    def loadimage(self, selected_figure):
        if selected_figure==1:
            #print "Loading image from figure 1"
            return self.fig1_image
        elif selected_figure==2:
            #print "Loading image from figure 2"
            return self.fig2_image
        else:
            print "Loading process failed"

    def saveimagefft(self, info, selected_figure):
        if selected_figure == 1:
            self.fig1_image_fft = info
            #print "Saved imagefft from figure 1"
        elif selected_figure == 2:
            self.fig2_image_fft = info
            #print "Saved imagefft from figure 2"
        else:
            print "Saving process failed"

    def loadimagefft(self, selected_figure):
        if selected_figure==1:
            #print "Loading imagefft from figure 1"
            return self.fig1_image_fft
        elif selected_figure==2:
            #print "Loading imagefft from figure 2"
            return self.fig2_image_fft
        else:
            print "Loading process failed"

    def saveq(self, info, selected_figure):
        if selected_figure == 1:
            self.fig1_q = info
            #print "Saved q from figure 1"
        elif selected_figure == 2:
            self.fig2_q = info
            #print "Saved q from figure 2"
        else:
            print "Saving process failed"

    def loadq(self, selected_figure):
        if selected_figure==1:
            #print "Loading q from figure 1"
            return self.fig1_q
        elif selected_figure==2:
            #print "Loading q from figure 2"
            return self.fig2_q
        else:
            print "Loading process failed"

    def savephase_uncorr(self, info, selected_figure):
        if selected_figure == 1:
            self.fig1_phase_uncorr = info
            #print "Saved Phase uncorrected from figure 1"
        elif selected_figure == 2:
            self.fig2_phase_uncorr = info
            #print "Saved Phase uncorrected from figure 2"
        else:
            print "Saving process failed"

    def loadphase_uncorr(self, selected_figure):
        if selected_figure==1:
            #print "Loading Phase uncorrected from figure 1"
            return self.fig1_phase_uncorr
        elif selected_figure==2:
            #print "Loading Phase uncorrected from figure 2"
            return self.fig2_phase_uncorr
        else:
            print "Loading process failed"

    def savephase_corr(self, info, selected_figure):
        if selected_figure == 1:
            self.fig1_phase_corr = info
            #print "Saved Phase corrected from figure 1"
        elif selected_figure == 2:
            self.fig2_phase_corr = info
            #print "Saved Phase corrected from figure 2"
        else:
            print "Saving process failed"

    def loadphase_corr(self, selected_figure):
        if selected_figure == 1:
            #print "Loading Phase corrected from figure 1"
            return self.fig1_phase_corr
        elif selected_figure == 2:
            #print "Loading Phase corrected from figure 2"
            return self.fig2_phase_corr
        else:
            print "Loading process failed"

    def savederivephase(self, info, selected_figure):
        if selected_figure == 1:
            self.fig1_derive_phase = info
            #print "Saved Phase Derivative from figure 1"
        elif selected_figure == 2:
            self.fig2_derive_phase = info
            #print "Saved Phase Derivative from figure 2"
        else:
            print "Saving process failed"

    def loadderivephase(self, selected_figure):
        if selected_figure==1:
            #print "Loading Phase Derivative from figure 1"
            return self.fig1_derive_phase
        elif selected_figure==2:
            #print "Loading Phase Derivative from figure 2"
            return self.fig2_derive_phase
        else:
            print "Loading process failed"

    def savelineg(self, info, infog, infoa, selected_figure):
        if selected_figure == 1:
            self.gpoint_fig1 = info
            self.ginfofig1 = infog
            self.a_lattice = infoa
            #print "Saved the coordinate of g line drawn in figure 1 and lattice a"
        elif selected_figure == 2:
            self.gpoint_fig2 = info
            self.ginfofig2 = infog
            self.a_lattice = infoa
            #print "Saved the coordinate of g line drawn in figure 2 and lattice a"
        else:
            print "Saving process failed"

    def loadlineg(self, selected_figure):
        if selected_figure==1:
            #print "Loading the coordinate of g line drawn from figure 1 and lattice a"
            return (self.gpoint_fig1, self.ginfofig1, self.a_lattice)
        elif selected_figure==2:
            #print "Loading the coordinate of g line drawn from figure 2 and lattice a"
            return (self.gpoint_fig2, self.ginfofig2,self.a_lattice)
        else:
            print "Loading process failed"

    def save_g_vector_ref(self, info, selected_figure):
        if selected_figure == 1:
            self.g_ref_vector_fig1 = info
            #print "Saved the coordinate of g vector in pixel-1 in figure 1 (x,y) base"
        elif selected_figure == 2:
            self.g_ref_vector_fig2 = info
            #print "Saved the coordinate of g vector in pixel-1 in figure 2 (x,y) base"
        else:
            print "Saving process failed"

    def load_g_vector_ref(self, selected_figure):
        if selected_figure==1:
            #print "Loading the coordinate of g vector in pixel-1 in figure 1 (x,y) base"
            return self.g_ref_vector_fig1
        elif selected_figure==2:
            #print "Loading the coordinate of g vector in pixel-1 in figure 2 (x,y) base"
            return self.g_ref_vector_fig2
        else:
            print "Loading process failed"




# --------------------------------------------
# Information
# --------------------------------------------
#class information:
#    def __init__(self):
#        garbage=0

# --------------------------------------------
# GUI
# --------------------------------------------

class GUI():
    # Variable used to communicate with the UI
    def __init__(self):
        self.selected_figure = 1
        self.position1 = [0, 0]
        self.radius1 = 0
        self.position1 = [0, 0]
        self.position2 = [0, 0]
        self.radius1 = 0
        self.radius2 = 0
        self.circle1 = plt.Circle(self.position1, self.radius1, color='b', fill=False)
        self.circle2 = plt.Circle(self.position2, self.radius2, color='b', fill=False)
        self.position_rectangle1 = [(0, 0), (0, 0)] #top-left and bottom-right
        self.position_rectangle2 = [(0, 0), (0, 0)]
        self.rectangle1 = patch.Rectangle((0, 0), 0, 0)
        self.rectangle2 = patch.Rectangle((0, 0), 0, 0)
        self.linegfig1 = [[0, 0], [0, 0]]
        self.linegfig2 = [[0, 0], [0, 0]]
        self.ginfofig1 = [[0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0]]
        self.ginfofig2 = [[0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0]]
        self.line1 = lines.Line2D(self.linegfig1[0],self.linegfig1[1], color='r')
        self.line2 = lines.Line2D(self.linegfig2[0], self.linegfig2[1], color='r')
        self.line1_acquired = patch.Arrow(0,0,0,0, color='b')
        self.line2_acquired = patch.Arrow(0,0,0,0, color='b')

    def start(self,darkholo):
        self.darkholo = darkholo

        # Layout in matplotlib
        fig1 = plt.figure(1)
        self.fig1ax1 = fig1.add_subplot(2, 3, 1)
        self.fig1ax1.add_line(self.line1)
        self.fig1ax1.add_patch(self.line1_acquired)
        self.fig1ax1.annotate('DFH image', xy=(.5, 1.085), xycoords='axes fraction', horizontalalignment='center',
                          verticalalignment='top', fontsize=12)
        self.fig1ax2 = fig1.add_subplot(2, 3, 4)
        self.fig1ax2.annotate('FFT', xy=(.5, 1.085), xycoords='axes fraction', horizontalalignment='center',
                          verticalalignment='top', fontsize=12)
        self.fig1ax7 = fig1.add_subplot(2, 3, 2)
        self.fig1ax7.annotate('Amplitude', xy=(.5, 1.085), xycoords='axes fraction', horizontalalignment='center',
                          verticalalignment='top', fontsize=12)
        self.fig1ax8 = fig1.add_subplot(2, 3, 5)
        self.fig1ax8.annotate('Phase', xy=(.5, 1.085), xycoords='axes fraction', horizontalalignment='center',
                          verticalalignment='top', fontsize=12)
        self.fig1ax8.add_patch(self.rectangle1)
        self.fig1ax9 = fig1.add_subplot(2, 3, 3)
        self.fig1ax9.annotate('Derive Phase -- Parallel g', xy=(.5, 1.085), xycoords='axes fraction', horizontalalignment='center',
                          verticalalignment='top', fontsize=12)
        self.fig1ax10 = fig1.add_subplot(2, 3, 6)
        self.fig1ax10.annotate('Derive Phase -- Perp g', xy=(.5, 1.085), xycoords='axes fraction', horizontalalignment='center',
                          verticalalignment='top', fontsize=12)

        fig2 = plt.figure(2)
        self.fig2ax1 = fig2.add_subplot(2, 3, 1)
        self.fig2ax1.add_line(self.line2)
        self.fig2ax1.add_patch(self.line2_acquired)
        self.fig2ax1.annotate('DFH image', xy=(.5, 1.085), xycoords='axes fraction', horizontalalignment='center',
                          verticalalignment='top', fontsize=12)
        self.fig2ax2 = fig2.add_subplot(2, 3, 4)
        self.fig2ax2.annotate('FFT', xy=(.5, 1.085), xycoords='axes fraction', horizontalalignment='center',
                          verticalalignment='top', fontsize=12)
        self.fig2ax7 = fig2.add_subplot(2, 3, 2)
        self.fig2ax7.annotate('Amplitude', xy=(.5, 1.085), xycoords='axes fraction', horizontalalignment='center',
                          verticalalignment='top', fontsize=12)
        self.fig2ax8 = fig2.add_subplot(2, 3, 5)
        self.fig2ax8.annotate('Phase', xy=(.5, 1.085), xycoords='axes fraction', horizontalalignment='center',
                          verticalalignment='top', fontsize=12)
        self.fig2ax8.add_patch(self.rectangle2)
        self.fig2ax9 = fig2.add_subplot(2, 3, 3)
        self.fig2ax9.annotate('Derive Phase -- Parallel g', xy=(.5, 1.085), xycoords='axes fraction', horizontalalignment='center',
                          verticalalignment='top', fontsize=12)
        self.fig2ax10 = fig2.add_subplot(2, 3, 6)
        self.fig2ax10.annotate('Derive Phase -- Perp g', xy=(.5, 1.085), xycoords='axes fraction', horizontalalignment='center',
                          verticalalignment='top', fontsize=12)

        # Another figure with buttons
        figbutton = plt.figure(3, figsize=(1.2,3.5))
        self.buttonax1 = figbutton.add_subplot(7,1,1)
        self.buttonax2 = figbutton.add_subplot(7,1,2)
        self.buttonax3 = figbutton.add_subplot(7,1,3)
        self.buttonax4 = figbutton.add_subplot(7,1,4)
        self.buttonax5 = figbutton.add_subplot(7,1,6)
        self.buttonax6 = figbutton.add_subplot(7,1,5)
        self.buttonax7 = figbutton.add_subplot(7,1,7)

        figbutton_button1 = Button(self.buttonax1, 'Open')
        figbutton_button1.on_clicked(self.openfile)
        figbutton_button2 = Button(self.buttonax2, 'FFT')
        figbutton_button2.on_clicked(self.fft)
        figbutton_button3 = Button(self.buttonax3, 'Mask')
        figbutton_button3.on_clicked(self.mask)
        figbutton_button4 = Button(self.buttonax4, 'Derive')
        figbutton_button4.on_clicked(self.derive)
        figbutton_button5 = RadioButtons(self.buttonax5, ('Figure 1', 'Figure 2'))
        figbutton_button5.on_clicked(self.choose_figure)
        figbutton_button6 = Button(self.buttonax6, 'Draw g')
        figbutton_button6.on_clicked(self.drawlineg)
        figbutton_button7 = Button(self.buttonax7, 'Strain tensor')
        figbutton_button7.on_clicked(self.tensor)

        # Fig 4 = Tk window for entry widget

        fig4 = Tkinter.Toplevel(window_DFH)
        fig4.title("User inputs")
        self.a_lattice_mess = Tkinter.Label(fig4, text="Lattice parameter in Angstrom")
        self.dfh_g_mess = Tkinter.Label(fig4, text="g dark-field hologram")
        self.dfh_g_mess_h = Tkinter.Label(fig4, text="h:")
        self.dfh_g_mess_k = Tkinter.Label(fig4, text="k:")
        self.dfh_g_mess_l = Tkinter.Label(fig4, text="l:")
        self.draw_g_mess = Tkinter.Label(fig4, text="g coresponding to x")
        self.draw_g_mess_h = Tkinter.Label(fig4, text="h:")
        self.draw_g_mess_k = Tkinter.Label(fig4, text="k:")
        self.draw_g_mess_l = Tkinter.Label(fig4, text="l:")
        self.zaxis_g_mess = Tkinter.Label(fig4, text="zone axis")
        self.zaxis_g_mess_h = Tkinter.Label(fig4, text="h:")
        self.zaxis_g_mess_k = Tkinter.Label(fig4, text="k:")
        self.zaxis_g_mess_l = Tkinter.Label(fig4, text="l:")

        # Entry Lattice parameter
        self.a_lattice = Tkinter.Entry(fig4)

        # Entry (hkl) of darkfield hologram acquired
        self.dfh_g_h = Tkinter.Entry(fig4, width=4)
        self.dfh_g_k = Tkinter.Entry(fig4, width=4)
        self.dfh_g_l = Tkinter.Entry(fig4, width=4)

        # Entry (hkl) of g drawn
        self.draw_g_h = Tkinter.Entry(fig4, width=4)
        self.draw_g_k = Tkinter.Entry(fig4, width=4)
        self.draw_g_l = Tkinter.Entry(fig4, width=4)

        # Entry (hkl) of zone axis
        self.zaxis_g_h = Tkinter.Entry(fig4, width=4)
        self.zaxis_g_k = Tkinter.Entry(fig4, width=4)
        self.zaxis_g_l = Tkinter.Entry(fig4, width=4)

        #Layout
        self.a_lattice_mess.grid(row=0, column=0)
        self.dfh_g_mess.grid(row=1, column=0)
        self.draw_g_mess.grid(row=2, column=0)
        self.zaxis_g_mess.grid(row=3, column=0)
        self.a_lattice.grid(row=0, column=1, columnspan=6)
        self.dfh_g_mess_h.grid(row=1, column=1)
        self.dfh_g_h.grid(row=1, column=2)
        self.dfh_g_mess_k.grid(row=1, column=3)
        self.dfh_g_k.grid(row=1, column=4)
        self.dfh_g_mess_l.grid(row=1, column=5)
        self.dfh_g_l.grid(row=1, column=6)
        self.draw_g_mess_h.grid(row=2, column=1)
        self.draw_g_h.grid(row=2, column=2)
        self.draw_g_mess_k.grid(row=2, column=3)
        self.draw_g_k.grid(row=2, column=4)
        self.draw_g_mess_l.grid(row=2, column=5)
        self.draw_g_l.grid(row=2, column=6)
        self.zaxis_g_mess_h.grid(row=3, column=1)
        self.zaxis_g_h.grid(row=3, column=2)
        self.zaxis_g_mess_k.grid(row=3, column=3)
        self.zaxis_g_k.grid(row=3, column=4)
        self.zaxis_g_mess_l.grid(row=3, column=5)
        self.zaxis_g_l.grid(row=3, column=6)

        #canvas.show()
        #canvas.get_tk_widget().pack(side=Tkinter.TOP, fill=Tkinter.BOTH, expand=1)


        # Create the connection between the interface and user to get the events
        fig1.canvas.mpl_connect('button_press_event', self.change_circle)
        fig1.canvas.mpl_connect('key_press_event', self.change_posrad)
        fig1.canvas.mpl_connect('key_press_event', self.defline)
        fig1.canvas.mpl_connect('button_press_event', self.first_ref)
        fig1.canvas.mpl_connect('button_release_event',self.draw_ref)

        fig2.canvas.mpl_connect('button_press_event', self.change_circle)
        fig2.canvas.mpl_connect('key_press_event', self.change_posrad)
        fig2.canvas.mpl_connect('key_press_event', self.defline)
        fig2.canvas.mpl_connect('button_press_event', self.first_ref)
        fig2.canvas.mpl_connect('button_release_event',self.draw_ref)

        # Call the closeprogram function to close the whole program properly
        fig1.canvas.mpl_connect('close_event', GUI.closeprogram)

        plt.show()

    # Open file function ==> Calling darkholo.openimage function and results in showing the image in the good figure
    def openfile(self,event):
        filepath = tkFileDialog.askopenfilename(title="Open an image")
        image_open = darkholo.openimage(filepath,self.selected_figure)
        plt.figure(self.selected_figure) # Activate the selected figure
        if self.selected_figure ==1:
            self.fig1ax1.imshow(image_open, cmap="gray")
        elif self.selected_figure ==2:
            self.fig2ax1.imshow(image_open, cmap='gray')
        plt.draw() # Needed to refresh the selected figure

    # Make the fft and display it on both figures accordingly
    def fft(self, event):
        image_fft = darkholo.fft(self.selected_figure)[0]
        plt.figure(self.selected_figure) # activate selected figure
        if self.selected_figure ==1:
            # Show the image in log scale (very large intensity)
            self.fig1ax2.imshow(numpy.log1p(image_fft), cmap="gray")
            # Draw the first circle on the middle of the FFT
            self.radius1 = darkholo.fft(self.selected_figure)[1] / 10.0
            self.position1 = (darkholo.fft(self.selected_figure)[1]/ 2,darkholo.fft(self.selected_figure)[2]/ 2)
            self.circle1 = plt.Circle(self.position1, self.radius1, color='b', fill=False)
            self.fig1ax2.add_artist(self.circle1)
            plt.draw() # Needed to refresh the selected figure
        elif self.selected_figure ==2:
            # Show the image in log scale (very large intensity)
            self.fig2ax2.imshow(numpy.log1p(image_fft), cmap="gray")
            # Draw the first circle on the middle of the FFT
            self.radius2 = darkholo.fft(self.selected_figure)[1] / 10.0
            self.position2 = (darkholo.fft(self.selected_figure)[1]/ 2,darkholo.fft(self.selected_figure)[2]/ 2)
            self.circle2 = plt.Circle(self.position2, self.radius2, color='b', fill=False)
            self.fig2ax2.add_artist(self.circle2)
            plt.draw() # Needed to refresh the selected figure

    # Define the new position and the new radius of the circle to mask the FFT
    # Adapted to the selection of figures
    def change_posrad(self, event):
        if self.selected_figure == 1:
            # Issue with the event.inaxes to be investigated
            # print event.key
            # print event.key == 'a'
            # print self.fig1ax2 == event.inaxes
            # print event.inaxes
            # print self.fig1ax2
            if event.key == 'a' and self.fig1ax2 == event.inaxes:
                self.position1 = (event.xdata, event.ydata)
                print "position= ", self.position1
            if event.key == 'z' and self.fig1ax2 == event.inaxes:
                self.radius1 = numpy.sqrt((event.xdata - self.position1[0]) ** 2 + (event.ydata - self.position1[1]) ** 2)
                print "r= ", self.radius1
        elif self.selected_figure == 2:
            # Issue with the event.inaxes to be investigated
            # print event.key
            # print event.key == 'a'
            # print self.fig2ax2 == event.inaxes
            # print event.inaxes
            # print self.fig2ax2
            if event.key == 'a' and self.fig2ax2 == event.inaxes:
                self.position2 = (event.xdata, event.ydata)
                print "position= ", self.position2
            if event.key == 'z' and self.fig2ax2 == event.inaxes:
                self.radius2 = numpy.sqrt((event.xdata - self.position2[0]) ** 2 + (event.ydata - self.position2[1]) ** 2)
                print "r= ", self.radius2

    # Get the information of the circle drawn to perform the mask and/or operation
    # In --- SELF
    # Out --- Tuple (position, radius, figure)
    def getcircle(self):
        if self.selected_figure ==1:
            circle_position = self.position1
            circle_radius = self.radius1
            figure = self.selected_figure
            return(circle_position, circle_radius, figure)
        elif self.selected_figure ==2:
            circle_position = self.position2
            circle_radius = self.radius2
            figure = self.selected_figure
            return (circle_position, circle_radius, figure)

    # Draw a circle in the new position with the new radius (re-centered at the maximum intensity)
    # Adapted to the figure selected
    # Circle removed and replaced accordingly
    def change_circle(self, event):
        if self.selected_figure ==1:
            #print event.button == 1
            #print self.fig1ax2 == event.inaxes
            #print event.inaxes
            if event.button == 1 and self.fig1ax2 == event.inaxes:
                self.circle1.remove()
                circle1 = GUI.getcircle(self)
                position_corr = darkholo.max_circle(circle1[0],circle1[1],circle1[2])
                self.position1 = position_corr
                print self.position1, self.radius1
                self.circle1 = plt.Circle(self.position1, self.radius1, color='b', fill=False)
                self.fig1ax2.add_artist(self.circle1)
                event.canvas.draw()
                print("Circle drawn")
        elif self.selected_figure ==2:
            if event.button == 1 and self.fig2ax2 == event.inaxes:
                self.circle2.remove()
                circle2 = GUI.getcircle(self)
                position_corr = darkholo.max_circle(circle2[0],circle2[1],circle2[2])
                self.position2 = position_corr
                print self.position2, self.radius2
                self.circle2 = plt.Circle(self.position2, self.radius2, color='b', fill=False)
                self.fig2ax2.add_artist(self.circle2)
                event.canvas.draw()
                print("Circle drawn")

    # Draw the masked FFT and display it on the good figure
    def mask(self, event):
        if self.selected_figure ==1:
            masking = darkholo.mask()
            fft_masked = masking[0]
            amplitude = masking[1]
            phase = masking[2]
            corr = masking[5]
            phase_uncor = masking[3]
            phase_uncor_unwrap = masking[4]
            plt.figure(self.selected_figure)
            self.fig1ax2.imshow(numpy.log1p(numpy.abs(fft_masked)), cmap="gray")
            print "masked"
            self.fig1ax7.imshow(numpy.log10(amplitude), cmap="gray")
            self.fig1ax8.imshow(phase, cmap="gray")
            plt.draw()
            fig2phase = plt.figure()
            fig2phase2 = plt.figure()
            fig2phase.ax = fig2phase.add_subplot(2, 2, 1)
            fig2phase.axx = fig2phase.add_subplot(2, 2, 2)
            fig2phase.axxx = fig2phase.add_subplot(2, 2, 3)
            fig2phase.axxxx = fig2phase.add_subplot(2, 2, 4)
            fig2phase2.ax = fig2phase2.add_subplot(1, 2, 1)
            fig2phase2.axx = fig2phase2.add_subplot(1, 2, 2)
            fig2phase.ax.imshow(phase, cmap="gray")
            fig2phase.axx.imshow(phase_uncor, cmap="gray")
            fig2phase.axxx.imshow(phase_uncor_unwrap, cmap="gray")
            fig2phase.axxxx.imshow(corr, cmap="gray")
            fig2phase2.ax.imshow(phase_uncor, cmap="gray")
            fig2phase2.axx.imshow(corr, cmap="gray")
            fig2phase.show()
            fig2phase2.show()
            plt.figure(self.selected_figure)
        elif self.selected_figure ==2:
            masking = darkholo.mask()
            fft_masked = masking[0]
            amplitude = masking[1]
            phase = masking[2]
            plt.figure(self.selected_figure)
            self.fig2ax2.imshow(numpy.log1p(numpy.abs(fft_masked)), cmap="gray")
            print "masked"
            self.fig2ax7.imshow(numpy.log10(amplitude), cmap="gray")
            self.fig2ax8.imshow(phase, cmap="gray")
            plt.draw()
            fig2phase = plt.figure()
            ax = plt.gca()
            ax.imshow(phase, cmap="gray")
            fig2phase.show()
            plt.figure(self.selected_figure)

    # Define the position of the g line known on the image (calibration purpose)
    def defline(self, event):
        if self.selected_figure ==1:
            if event.key == 'a' and self.fig1ax1 == event.inaxes:
                self.linegfig1[0] = (event.xdata, event.ydata)
                print self.linegfig1
            elif event.key == 'z' and self.fig1ax1 == event.inaxes:
                self.linegfig1[1] = (event.xdata, event.ydata)
                print self.linegfig1
        elif self.selected_figure ==2:
            if event.key == 'a' and self.fig2ax1 == event.inaxes:
                self.linegfig2[0] = (event.xdata, event.ydata)
                print self.linegfig2
            elif event.key == 'z' and self.fig2ax1 == event.inaxes:
                self.linegfig2[1] = (event.xdata, event.ydata)
                print self.linegfig2

    # Draw the known g line to fix the calibration (rotationwise speaking)
    def drawlineg(self,event):
        if self.selected_figure ==1:
            plt.figure(self.selected_figure)
            self.line1.remove()
            self.line1_acquired.remove()
            self.line1 = lines.Line2D([self.linegfig1[0][0],self.linegfig1[1][0]],
                                      [self.linegfig1[0][1],self.linegfig1[1][1]], color='r')
            gline = self.linegfig1
            self.ginfofig1[0]=[int(self.draw_g_h.get()),int(self.draw_g_k.get()),int(self.draw_g_l.get())]
            self.ginfofig1[1]=[int(self.dfh_g_h.get()),int(self.dfh_g_k.get()),int(self.dfh_g_l.get())]
            self.ginfofig1[2]=[int(self.zaxis_g_h.get()),int(self.zaxis_g_k.get()),int(self.zaxis_g_l.get())]
            self.ginfofig1[3]=numpy.cross(self.ginfofig1[2],self.ginfofig1[0])
            print self.ginfofig1[3]
            ginfo = self.ginfofig1
            a = float(self.a_lattice.get())
            print "Darkfield hologram g (hkl): ", self.ginfofig1[1]
            print "g drawn (hkl): ", self.ginfofig1[0]
            print "zone axis (hkl): ", self.ginfofig1[2]
            print "3rd axis (hkl): ", self.ginfofig1[3]
            g_ref = darkholo.gline(gline,ginfo,a,self.selected_figure)[1]
            self.line1_acquired=patch.FancyArrow(g_ref[0],g_ref[1],g_ref[2],g_ref[3], color='b')
            self.fig1ax1.add_line(self.line1)
            self.fig1ax1.add_patch(self.line1_acquired)
            plt.draw()
        if self.selected_figure ==2:
            plt.figure(self.selected_figure)
            self.line2.remove()
            self.line2_acquired.remove()
            self.line2 = lines.Line2D([self.linegfig2[0][0], self.linegfig2[1][0]],
                                      [self.linegfig2[0][1], self.linegfig2[1][1]], color='r')
            gline = self.linegfig2
            self.ginfofig2[0] = [int(self.draw_g_h.get()),int(self.draw_g_k.get()),int(self.draw_g_l.get())]
            self.ginfofig2[1] = [int(self.dfh_g_h.get()),int(self.dfh_g_k.get()),int(self.dfh_g_l.get())]
            self.ginfofig2[2] = [int(self.zaxis_g_h.get()),int(self.zaxis_g_k.get()),int(self.zaxis_g_l.get())]
            self.ginfofig2[3] = numpy.cross(self.ginfofig2[2], self.ginfofig2[0])
            ginfo = self.ginfofig2
            a = float(self.a_lattice.get())
            g_ref = darkholo.gline(gline,ginfo,a,self.selected_figure)[1]
            self.line2_acquired = patch.FancyArrow(g_ref[0], g_ref[1], g_ref[2], g_ref[3], color='b')
            self.fig2ax1.add_line(self.line2)
            self.fig2ax1.add_patch(self.line2_acquired)
            plt.draw()


    # Draw the corner of the reference area on the phase image
    def first_ref(self,event):
        if self.selected_figure ==1:
                if event.button == 1 and self.fig1ax8 == event.inaxes:
                    self.rectangle1.remove()
                    self.position_rectangle1[0] = (event.xdata, event.ydata)
                    print self.position_rectangle1
                    print "First point saved"
        elif self.selected_figure ==2:
                if event.button == 1 and self.fig2ax8 == event.inaxes:
                    self.rectangle2.remove()
                    self.position_rectangle2[0] = (event.xdata, event.ydata)
                    print self.position_rectangle2
                    print "First point saved"

    # Draw the reference area on the phase image
    def draw_ref(self,event):
        if self.selected_figure ==1:
                if event.button == 1 and self.fig1ax8 == event.inaxes:
                    self.position_rectangle1[1] = (event.xdata, event.ydata)
                    print self.position_rectangle1
                    print "Second point saved"
                    conversion = (self.position_rectangle1[0][0],self.position_rectangle1[0][1])
                    width = numpy.abs(self.position_rectangle1[1][0]-self.position_rectangle1[0][0])
                    height = numpy.abs(self.position_rectangle1[1][1]-self.position_rectangle1[0][1])
                    print conversion
                    self.rectangle1 = patch.Rectangle(conversion, width, height, fill=False, edgecolor='green')
                    plt.figure(self.selected_figure)
                    self.fig1ax8.add_patch(self.rectangle1)
                    phase_cor = darkholo.reference(self.selected_figure)
                    self.fig1ax8.imshow(phase_cor, cmap="gray")
                    plt.draw()
                    # see the extraction
                    #fig = plt.figure()
                    #ax = plt.gca()
                    #ax.imshow(phase_cor, cmap="gray")
                    #fig.show()
                    #plt.figure(self.selected_figure)
        elif self.selected_figure ==2:
                if event.button == 1 and self.fig2ax8 == event.inaxes:
                    self.position_rectangle2[1] = (event.xdata, event.ydata)
                    print self.position_rectangle2
                    print "Second point saved"
                    conversion = (self.position_rectangle2[0][0],self.position_rectangle2[0][1])
                    width = numpy.abs(self.position_rectangle2[1][0]-self.position_rectangle2[0][0])
                    height = numpy.abs(self.position_rectangle2[0][1]-self.position_rectangle2[1][1])
                    print conversion
                    self.rectangle2 = patch.Rectangle(conversion, width, height, fill=False, edgecolor='green')
                    plt.figure(self.selected_figure)
                    self.fig2ax8.add_patch(self.rectangle2)
                    phase_cor = darkholo.reference(self.selected_figure)
                    self.fig2ax8.imshow(phase_cor, cmap="gray")
                    plt.draw()
                    # see the extraction
                    #fig = plt.figure()
                    #ax = plt.gca()
                    #ax.imshow(phase_cor, cmap="gray")
                    #fig.show()
                    #plt.figure(self.selected_figure)

     # Get the position of the reference for darkholo controller
    def getreference(self):
        if self.selected_figure ==1:
            reference= self.position_rectangle1
            figure = self.selected_figure
            return(reference, figure)
        elif self.selected_figure ==2:
            reference = self.position_rectangle2
            figure = self.selected_figure
            return (reference, figure)

    # Function to derive the phase and plot the derivative images
    def derive(self, event):
        #plt.figure(self.selected_figure)
        #self.lineg = matplotlib.lines.Line2D([darkholo.gpoint[0][0], darkholo.gpoint[1][0]],
        #                                     [darkholo.gpoint[0][1], darkholo.gpoint[1][1]], color='r')
        #self.fig1ax1.add_artist(self.lineg)
        derive_phase = darkholo.derivative(self.selected_figure)
        if self.selected_figure ==1:
            plt.figure(self.selected_figure)
            self.fig1ax9.imshow(derive_phase[2], cmap="bwr", vmin=-0.1, vmax=0.1)
            self.fig1ax10.imshow(derive_phase[3], cmap="bwr", vmin=-0.1, vmax=0.1)
            plt.draw()
            fig4 = plt.figure()
            axxx = fig4.add_subplot(121)
            axxx.imshow(derive_phase[0], cmap="bwr", vmin=-0.02, vmax=0.02)
            axxxx = fig4.add_subplot(122)
            axxxx.imshow(derive_phase[1], cmap="bwr", vmin=-0.02, vmax=0.02)
            fig4.show()
            plt.figure(self.selected_figure)
            print 'Derivative done'
        elif self.selected_figure ==2:
            plt.figure(self.selected_figure)
            self.fig2ax9.imshow(derive_phase[2], cmap="bwr", vmin=-0.1, vmax=0.1)
            self.fig2ax10.imshow(derive_phase[3], cmap="bwr", vmin=-0.1, vmax=0.1)
            plt.draw()
            fig4 = plt.figure()
            axxx = fig4.add_subplot(121)
            axxx.imshow(derive_phase[0], cmap="bwr", vmin=-0.02, vmax=0.02)
            axxxx = fig4.add_subplot(122)
            axxxx.imshow(derive_phase[1], cmap="bwr", vmin=-0.02, vmax=0.02)
            fig4.show()
            plt.figure(self.selected_figure)
            print 'Derivative done'

    # Show strain tensor
    def tensor(self,event):
        strain_result = darkholo.strain_tensor()
        fig_strain = plt.figure()
        a_exx = fig_strain.add_subplot(2,2,1)
        a_exx.annotate('Exx', xy=(.5, 1.085), xycoords='axes fraction', horizontalalignment='center',
                              verticalalignment='top', fontsize=12)
        a_exx.imshow(strain_result[0][0][0], cmap="bwr", vmin=-0.02, vmax=0.02)
        a_eyy = fig_strain.add_subplot(2,2,2)
        a_eyy.annotate('Eyy', xy=(.5, 1.085), xycoords='axes fraction', horizontalalignment='center',
                              verticalalignment='top', fontsize=12)
        a_eyy.imshow(strain_result[0][1][1], cmap="bwr", vmin=-0.02, vmax=0.02)
        a_exy = fig_strain.add_subplot(2,2,3)
        a_exy.annotate('Exy or Rxy ?', xy=(.5, 1.085), xycoords='axes fraction', horizontalalignment='center',
                                  verticalalignment='top', fontsize=12)
        a_exy.imshow(strain_result[0][0][1], cmap="bwr", vmin=-0.02, vmax=0.02)
        a_rxy = fig_strain.add_subplot(2,2,4)
        a_rxy.annotate('Exy or Rxy ?', xy=(.5, 1.085), xycoords='axes fraction', horizontalalignment='center',
                                  verticalalignment='top', fontsize=12)
        a_rxy.imshow(strain_result[1][0][1], cmap="bwr", vmin=-0.02, vmax=0.02)
        fig_strain.show()

    # Figure 1 or Figure 2 --- Selection of the radio button
    def choose_figure(self, label):
        figure_dict = {'Figure 1': 1, 'Figure 2': 2}
        self.selected_figure = figure_dict[label]
        plt.figure(self.selected_figure)
        print self.selected_figure
        return self.selected_figure

    # Proper way to close the pgm on the MAC without erros.
    def closeprogram(event):
        print "Closing pgm"
        plt.close('all')
        window_DFH.destroy()
        window_DFH.quit()


darkholo = darkholo()
storage = storage()
myGUI = GUI()
myGUI.start(darkholo)
window_DFH.mainloop()