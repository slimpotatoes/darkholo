# Darkholo software : Process darkfield holograms to get 2D deformation field and 2D strain tensor

# --------------------------------------------
# GUI // I/O libraries
# --------------------------------------------

# GUI Tkinter
import Tkinter
# Open file dialog
import tkFileDialog

# Need to open a tk-window else Mac crash
window_DFH = Tkinter.Tk()
# Hide the window
window_DFH.withdraw()

# Matplotlib libraries for GUI
import matplotlib
import matplotlib.lines
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.widgets import Button
from matplotlib.widgets import RadioButtons
# DM3 reader
import dm3_lib

# --------------------------------------------
# Processing libraries
# --------------------------------------------

# Mathematical library for 2D array processing
import numpy


# PENSER A ENLEVER LES VARIABLES GLOBALES,
# 1 Reorganiser en 3 parties : UI, Proc, script
# 2 designer les fonction (entree sortie)
# 3 simplifier les actions
# 4 Data storage (a penser)

# --------------------------------------------
# Controler Darkholo
# --------------------------------------------

class darkholo:
    def __init__(self):
        # Initialization of variables ---------
        # Derivative of the phase image along x and y directions
        self.phase_derivative0 = ""
        self.phase_derivative1 = ""

    # Open the image and store the image (function of the figure)
    # Input, self + filepath + the selected figure
    # Output the image.imagedate to be able to show it
    def openimage(self, filepath, selected_figure):
        image1 = dm3_lib.DM3(filepath)
        #self.height_image = self.image1.height
        #self.width_image = self.image1.width
        print image1
        print "Image loaded"
        print image1.info
        print "pixel size = %s %s" % image1.pxsize
        storage.saveimage(image1, selected_figure)
        return image1.imagedata

    # Perform the (Fast) Fourier Transform of the hologram and show the power spectrum shifted accordingly
    # Load image from the good figure
    # Save the fft of the image
    # Return the fft_image ready to be displayed
    def fft(self,selected_figure):
        image = storage.loadimage(selected_figure)
        image_fft = numpy.fft.fftshift(numpy.abs(numpy.fft.fft2(image.imagedata))**2)
        print("FFT performed")
        storage.saveimagefft(image_fft,selected_figure)
        return (image_fft,image.width,image.height) # return the tuple image, width and height

    # Create a mask on the circles area to isolate the interesting frequency and generate phase + amplitude images
    def mask(self):
        selected_figure = myGUI.getcircle()[2]
        image = storage.loadimage(selected_figure)
        g = storage.loadg(selected_figure)
        position = myGUI.getcircle()[0]
        radius = myGUI.getcircle()[1]
        fft_mask = numpy.zeros((image.width, image.height))
        correction = numpy.zeros((image.width, image.height))
        for i in range(0, image.width):
            for j in range(0, image.height):
                if ((i - position[1]) ** 2 + (j - position[0]) ** 2) < (radius ** 2):
                    fft_mask[i][j] = 1
        for k in range(0, image.width):
            for l in range(0, image.height):
                correction[k][l] = 2 * numpy.pi * (g[0] * k + g[1] * l)
        image_fft_masked = numpy.multiply(fft_mask, numpy.fft.fftshift(numpy.fft.fft2(image.imagedata)))
        amplitude1 = numpy.absolute(numpy.fft.ifft2((numpy.fft.ifftshift(image_fft_masked))))
        phase1 = numpy.angle(numpy.fft.ifft2((numpy.fft.ifftshift(image_fft_masked))))
        phase2 = phase1-correction-(numpy.round((phase1-correction)/2/numpy.pi))*2*numpy.pi
        storage.savephase(phase2,selected_figure)
        return (image_fft_masked, amplitude1, phase2)

    # Find the maximum of the circle and define norm of gx and gy
    # Input position, radius from GUI and selected_figure
    # Load image and image_fft
    # Correct the position of the circle on the maximum
    # Calculate and save the new position of g.
    def max_circle(self,position,radius,selected_figure):
        image = storage.loadimage(selected_figure)
        image_fft = storage.loadimagefft(selected_figure)
        k = 0
        l = 0
        for i in range(0, image.width):
            for j in range(0, image.height):
                if (((i - position[1]) ** 2 + (j - position[0]) ** 2) < (radius ** 2)) and \
                                image_fft[i][j] > image_fft[k][l]:
                    k = i
                    l = j
        position_corr = [l, k]
        gx = (position[1] - 0.5 * image.width) / image.width
        gy = (position[0] - 0.5 * image.height) / image.height
        g = [gx, gy]
        storage.saveg(g,selected_figure)
        return position_corr


    # Derivative of the phase ----- TO TRANSFORM TOMORROW -----------
    def derivative(self):
        derive_temp = numpy.gradient(numpy.exp(self.phase2 * 1j))
        print derive_temp
        print numpy.shape(derive_temp)
        self.phase_derivative0 = numpy.imag(numpy.multiply(numpy.exp(-self.phase2 * 1j), derive_temp[0]))
        self.phase_derivative1 = numpy.imag(numpy.multiply(numpy.exp(-self.phase2 * 1j), derive_temp[1]))
        print numpy.shape(self.phase_derivative0)

# --------------------------------------------
# Storage
# --------------------------------------------
class storage:
    def __init__(self):
        self.fig1_info = ''
        self.fig2_info = ''
        self.fig1_image = ''
        self.fig2_image = ''
        self.fig1_image_fft = ''
        self.fig2_image_fft = ''
        self.fig1_g = [0, 0]
        self.fig2_g = [0, 0]
        self.fig1_phase = ''
        self.fig2_phase = ''

    def saveinfo(self, info, selected_figure):
        if selected_figure ==1:
            self.fig1_info = info
            print "Saved info from figure 1"
        elif selected_figure ==2:
            self.fig2_info = info
            print "Saved info from figure 2"
        else:
            print "Saving process failed"

    def loadinfo(self, selected_figure):
        if selected_figure==1:
            print "Loading info from figure 1"
            return self.fig1_info
        elif selected_figure==2:
            print "Loading info from figure 2"
            return self.fig2_info
        else:
            print "Loading process failed"

    def saveimage(self, info, selected_figure):
        if selected_figure == 1:
            self.fig1_image = info
            print "Saved image from figure 1"
        elif selected_figure == 2:
            self.fig2_image = info
            print "Saved image from figure 2"
        else:
            print "Saving process failed"

    def loadimage(self, selected_figure):
        if selected_figure==1:
            print "Loading image from figure 1"
            return self.fig1_image
        elif selected_figure==2:
            print "Loading image from figure 2"
            return self.fig2_image
        else:
            print "Loading process failed"

    def saveimagefft(self, info, selected_figure):
        if selected_figure == 1:
            self.fig1_image_fft = info
            print "Saved imagefft from figure 1"
        elif selected_figure == 2:
            self.fig2_image_fft = info
            print "Saved imagefft from figure 2"
        else:
            print "Saving process failed"

    def loadimagefft(self, selected_figure):
        if selected_figure==1:
            print "Loading imagefft from figure 1"
            return self.fig1_image_fft
        elif selected_figure==2:
            print "Loading imagefft from figure 2"
            return self.fig2_image_fft
        else:
            print "Loading process failed"

    def saveg(self, info, selected_figure):
        if selected_figure == 1:
            self.fig1_g = info
            print "Saved imagefft from figure 1"
        elif selected_figure == 2:
            self.fig2_g = info
            print "Saved imagefft from figure 2"
        else:
            print "Saving process failed"

    def loadg(self, selected_figure):
        if selected_figure==1:
            print "Loading imagefft from figure 1"
            return self.fig1_g
        elif selected_figure==2:
            print "Loading imagefft from figure 2"
            return self.fig2_g
        else:
            print "Loading process failed"

    def savephase(self, info, selected_figure):
        if selected_figure == 1:
            self.fig1_phase = info
            print "Saved imagefft from figure 1"
        elif selected_figure == 2:
            self.fig2_phase = info
            print "Saved imagefft from figure 2"
        else:
            print "Saving process failed"

    def loadphase(self, selected_figure):
        if selected_figure==1:
            print "Loading imagefft from figure 1"
            return self.fig1_phase
        elif selected_figure==2:
            print "Loading imagefft from figure 2"
            return self.fig2_phase
        else:
            print "Loading process failed"

# --------------------------------------------
# Information
# --------------------------------------------
#class information:
#    def __init__(self):
#        garbage=0





# --------------------------------------------
# GUI
# --------------------------------------------

class GUI():
    def __init__(self):
        self.selected_figure = 1
        self.position1 = [0, 0]
        self.radius1 = 0
        self.position1 = [0, 0]
        self.position2 = [0, 0]
        self.radius1 = 0
        self.radius2 = 0
        self.circle1 = plt.Circle(self.position1, self.radius1, color='b', fill=False)
        self.circle2 = plt.Circle(self.position2, self.radius2, color='b', fill=False)

    def start(self,darkholo):
        self.darkholo = darkholo

        # Layout in matplotlib using GridSpec
        fig1 = plt.figure(1)
        self.fig1ax1 = fig1.add_subplot(2,2,1)
        self.fig1ax1.annotate('DFH image', xy=(.5, 1.1), xycoords='axes fraction', horizontalalignment='center',
                          verticalalignment='top', fontsize=12)
        self.fig1ax2 = fig1.add_subplot(2,2,3)
        self.fig1ax2.annotate('FFT', xy=(.5, 1.1), xycoords='axes fraction', horizontalalignment='center',
                          verticalalignment='top', fontsize=12)
        self.fig1ax7 = fig1.add_subplot(2,2,2)
        self.fig1ax7.annotate('Amplitude', xy=(.5, 1.1), xycoords='axes fraction', horizontalalignment='center',
                          verticalalignment='top', fontsize=12)
        self.fig1ax8 = fig1.add_subplot(2,2,4)
        self.fig1ax8.annotate('Phase', xy=(.5, 1.1), xycoords='axes fraction', horizontalalignment='center',
                          verticalalignment='top', fontsize=12)

        self.fig1ax1.axis('on')
        self.fig1ax2.axis('on')

        # Layout in matplotlib using GridSpec
        fig2 = plt.figure(2)
        self.fig2ax1 = fig2.add_subplot(2, 2, 1)
        self.fig2ax1.annotate('DFH image', xy=(.5, 1.1), xycoords='axes fraction', horizontalalignment='center',
                          verticalalignment='top', fontsize=12)
        self.fig2ax2 = fig2.add_subplot(2, 2, 3)
        self.fig2ax2.annotate('FFT', xy=(.5, 1.1), xycoords='axes fraction', horizontalalignment='center',
                          verticalalignment='top', fontsize=12)
        self.fig2ax7 = fig2.add_subplot(2, 2, 2)
        self.fig2ax7.annotate('Amplitude', xy=(.5, 1.1), xycoords='axes fraction', horizontalalignment='center',
                          verticalalignment='top', fontsize=12)
        self.fig2ax8 = fig2.add_subplot(2, 2, 4)
        self.fig2ax8.annotate('Phase', xy=(.5, 1.1), xycoords='axes fraction', horizontalalignment='center',
                          verticalalignment='top', fontsize=12)

        # Another figure with buttons
        figbutton = plt.figure(3, figsize=(1.2,3.5))
        self.buttonax1 = figbutton.add_subplot(5,1,1)
        self.buttonax2 = figbutton.add_subplot(5,1,2)
        self.buttonax3 = figbutton.add_subplot(5,1,3)
        self.buttonax4 = figbutton.add_subplot(5,1,4)
        self.buttonax5 = figbutton.add_subplot(5,1,5)

        figbutton_button1 = Button(self.buttonax1, 'Open')
        figbutton_button1.on_clicked(self.openfile)
        figbutton_button2 = Button(self.buttonax2, 'FFT')
        figbutton_button2.on_clicked(self.fft)
        figbutton_button3 = Button(self.buttonax3, 'Mask')
        figbutton_button3.on_clicked(self.mask)
        figbutton_button4 = Button(self.buttonax4, 'Draw g')
        figbutton_button4.on_clicked(self.drawg)
        figbutton_button5 = RadioButtons(self.buttonax5, ('Figure 1', 'Figure 2'))
        figbutton_button5.on_clicked(self.choose_figure)

        # Create the connection between the interface and user to get the events
        fig1.canvas.mpl_connect('button_press_event', self.change_circle)
        fig1.canvas.mpl_connect('key_press_event', self.change_posrad)
        fig1.canvas.mpl_connect('key_press_event', self.defgpoint)
        fig2.canvas.mpl_connect('button_press_event', self.change_circle)
        fig2.canvas.mpl_connect('key_press_event', self.change_posrad)
        fig2.canvas.mpl_connect('key_press_event', self.defgpoint)

        # Call the closeprogram function to close the whole program properly
        fig1.canvas.mpl_connect('close_event', GUI.closeprogram)

        plt.show()

    # Open file function ==> Calling darkholo.openimage function and results in showing the image in the good figure
    def openfile(self,event):
        filepath = tkFileDialog.askopenfilename(title="Open an image")
        image_open = darkholo.openimage(filepath,self.selected_figure)
        plt.figure(self.selected_figure) # Activate the selected figure
        if self.selected_figure ==1:
            self.fig1ax1.imshow(image_open, cmap="gray")
        elif self.selected_figure ==2:
            self.fig2ax1.imshow(image_open, cmap='gray')
        plt.draw() # Needed to refresh the selected figure

    # Make the fft and display it on both figures accordingly
    def fft(self, event):
        image_fft = darkholo.fft(self.selected_figure)[0]
        plt.figure(self.selected_figure) # activate selected figure
        if self.selected_figure ==1:
            # Show the image in log scale (very large intensity)
            self.fig1ax2.imshow(numpy.log1p(image_fft), cmap="gray")
            # Draw the first circle on the middle of the FFT
            self.radius1 = darkholo.fft(self.selected_figure)[1] / 10.0
            self.position1 = (darkholo.fft(self.selected_figure)[1]/ 2,darkholo.fft(self.selected_figure)[2]/ 2)
            self.circle1 = plt.Circle(self.position1, self.radius1, color='b', fill=False)
            self.fig1ax2.add_artist(self.circle1)
            plt.draw() # Needed to refresh the selected figure
        elif self.selected_figure ==2:
            # Show the image in log scale (very large intensity)
            self.fig2ax2.imshow(numpy.log1p(image_fft), cmap="gray")
            # Draw the first circle on the middle of the FFT
            self.radius2 = darkholo.fft(self.selected_figure)[1] / 10.0
            self.position2 = (darkholo.fft(self.selected_figure)[1]/ 2,darkholo.fft(self.selected_figure)[2]/ 2)
            self.circle2 = plt.Circle(self.position2, self.radius2, color='b', fill=False)
            self.fig2ax2.add_artist(self.circle2)
            plt.draw() # Needed to refresh the selected figure

    # Define the new position and the new radius of the circle to mask the FFT
    # Adapted to the selection of figures
    def change_posrad(self, event):
        if self.selected_figure == 1:
            if event.key == 'a' and self.fig1ax2 == event.inaxes:
                self.position1 = (event.xdata, event.ydata)
                print "position= ", self.position1
            if event.key == 'z' and self.fig1ax2 == event.inaxes:
                self.radius1 = numpy.sqrt((event.xdata - self.position1[0]) ** 2 + (event.ydata - self.position1[1]) ** 2)
                print "r= ", self.radius1
        elif self.selected_figure == 2:
            if event.key == 'a' and self.fig2ax2 == event.inaxes:
                self.position2 = (event.xdata, event.ydata)
                print "position= ", self.position2
            if event.key == 'z' and self.fig2ax2 == event.inaxes:
                self.radius2 = numpy.sqrt((event.xdata - self.position2[0]) ** 2 + (event.ydata - self.position2[1]) ** 2)
                print "r= ", self.radius2

    # Get the information of the circle drawn to perform the mask and/or operation
    # In --- SELF
    # Out --- Tuple (position, radius, figure)
    def getcircle(self):
        if self.selected_figure ==1:
            circle_position = self.position1
            circle_radius = self.radius1
            figure = self.selected_figure
            return(circle_position, circle_radius, figure)
        elif self.selected_figure ==2:
            circle_position = self.position2
            circle_radius = self.radius2
            figure = self.selected_figure
            return (circle_position, circle_radius, figure)

    # Draw a circle in the new position with the new radius (re-centered at the maximum intensity)
    # Adapted to the figure selected
    # Circle removed and replaced accordingly
    def change_circle(self, event):
        if self.selected_figure ==1:
            if event.button == 1 and self.fig1ax2 == event.inaxes:
                self.circle1.remove()
                circle1 = GUI.getcircle(self)
                position_corr = darkholo.max_circle(circle1[0],circle1[1],circle1[2])
                self.position1 = position_corr
                print self.position1, self.radius1
                self.circle1 = plt.Circle(self.position1, self.radius1, color='b', fill=False)
                self.fig1ax2.add_artist(self.circle1)
                event.canvas.draw()
                print("Circle drawn")
        elif self.selected_figure ==2:
            if event.button == 1 and self.fig2ax2 == event.inaxes:
                self.circle2.remove()
                circle2 = GUI.getcircle(self)
                position_corr = darkholo.max_circle(circle2[0],circle2[1],circle2[2])
                self.position2 = position_corr
                print self.position2, self.radius2
                self.circle2 = plt.Circle(self.position2, self.radius2, color='b', fill=False)
                self.fig2ax2.add_artist(self.circle2)
                event.canvas.draw()
                print("Circle drawn")

    # Draw the masked FFT and display it on the good figure
    def mask(self, event):
        if self.selected_figure ==1:
            masking = darkholo.mask()
            fft_masked = masking[0]
            amplitude = masking[1]
            phase = masking[2]
            plt.figure(self.selected_figure)
            self.fig1ax2.imshow(numpy.log1p(numpy.abs(fft_masked)), cmap="gray")
            print "masked"
            self.fig1ax7.imshow(numpy.log10(amplitude), cmap="gray")
            self.fig1ax8.imshow(phase, cmap="gray")
            plt.draw()
            fig2phase = plt.figure()
            ax = plt.gca()
            ax.imshow(phase, cmap="gray")
            fig2phase.show()
            plt.figure(self.selected_figure)
        elif self.selected_figure ==2:
            masking = darkholo.mask()
            fft_masked = masking[0]
            amplitude = masking[1]
            phase = masking[2]
            plt.figure(self.selected_figure)
            self.fig2ax2.imshow(numpy.log1p(numpy.abs(fft_masked)), cmap="gray")
            print "masked"
            self.fig2ax7.imshow(numpy.log10(amplitude), cmap="gray")
            self.fig2ax8.imshow(phase, cmap="gray")
            plt.draw()
            fig2phase = plt.figure()
            ax = plt.gca()
            ax.imshow(phase, cmap="gray")
            fig2phase.show()
            plt.figure(self.selected_figure)

    # TO BE COMMENTED WHEN NEEDED
    def defgpoint(self, event):
        if event.key == 'a' and self.fig1ax1 == event.inaxes:
            darkholo.gpoint[0] = (event.xdata, event.ydata)
            print darkholo.gpoint
        if event.key == 'z' and self.fig1ax1 == event.inaxes:
            darkholo.gpoint[1] = (event.xdata, event.ydata)
            print darkholo.gpoint

    # TO BE COMMENTED WHEN NEEDED
    def drawg(self, event):
        plt.figure(self.selected_figure)
        self.lineg = matplotlib.lines.Line2D([darkholo.gpoint[0][0], darkholo.gpoint[1][0]],
                                             [darkholo.gpoint[0][1], darkholo.gpoint[1][1]], color='r')
        self.fig1ax1.add_artist(self.lineg)
        darkholo.derivative()
        fig4 = plt.figure()
        axxx = fig4.add_subplot(121)
        axxx.imshow(darkholo.phase_derivative0, cmap="bwr", vmin=-0.1, vmax=0.1)
        axxxx = fig4.add_subplot(122)
        axxxx.imshow(darkholo.phase_derivative1, cmap="bwr", vmin=-0.1, vmax=0.1)
        fig4.show()
        plt.figure(self.selected_figure)
        print 'g line drawn'

    # Figure 1 or Figure 2 --- Selection of the radio button
    def choose_figure(self, label):
        figure_dict = {'Figure 1': 1, 'Figure 2': 2}
        self.selected_figure = figure_dict[label]
        plt.figure(self.selected_figure)
        print self.selected_figure
        return self.selected_figure

    # Proper way to close the pgm on the MAC without erros.
    def closeprogram(event):
        print "Closing pgm"
        plt.close('all')
        window_DFH.destroy()
        window_DFH.quit()


darkholo = darkholo()
storage = storage()
myGUI = GUI()
myGUI.start(darkholo)
window_DFH.mainloop()