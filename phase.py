import Tkinter
import tkFileDialog
from PIL import Image
from PIL import ImageTk

#Bug on Mac needed to open tk window before importing matplolib... weird
#window_DFH = Tkinter.Tk()
import matplotlib
import matplotlib.lines
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.widgets import Button
import dm3_lib
import numpy
import math
#window_DFH = Tkinter.Tk()
#window_DFH.withdraw()

def drawg(event):
    global point
    global ax
    global phase
    global transform
    global phase_derivative
    lineg = matplotlib.lines.Line2D([point[0][0],point[1][0]], [point[0][1],point[1][1]], color='r')
    print point[0][0]
    print point[1][0]
    ax.add_artist(lineg)
    transform=numpy.exp(phase*1j)
    derive_temp=numpy.gradient(transform)
    print numpy.shape(phase)
    print numpy.shape(transform)
    print numpy.shape(derive_temp)
    phase_derivative=numpy.imag(numpy.multiply(numpy.exp(-phase*1j),derive_temp[1]))
    fig4 = plt.figure()
    axxx = plt.gca()
    print numpy.shape(phase_derivative)
    axxx.imshow(phase_derivative, cmap="bwr",vmin=-0.1, vmax=0.1)
    fig4.show()
    print 'g line drawn'

def defpoint(event):
    global point
    global ax
    global ax1
    if event.key == 'a' and ax == event.inaxes:
        point[0]=(event.xdata, event.ydata)
        print point
    if event.key == 'z' and ax == event.inaxes:
        point[1]=(event.xdata, event.ydata)
        print point

point=[0,0]

phase=numpy.loadtxt('phase',delimiter=',')
f=plt.figure()
gs = gridspec.GridSpec(2,1)
#ax = plt.gca()
ax = plt.subplot(gs[0,0:1])
ax1 = plt.subplot(gs[1,0:1])

ax.imshow(phase, cmap="gray")
Button3 = Button(ax1,'Draw g')
Button3.on_clicked(drawg)

f.canvas.mpl_connect('key_press_event', defpoint)

# silicon structure
a=5.431
g220=numpy.sqrt(8)/a





plt.show()
#window_DFH.mainloop()