
import Tkinter
import tkFileDialog
from PIL import Image
from PIL import ImageTk

#Bug on Mac needed to open tk window before importing matplolit ... weird
window_DFH = Tkinter.Tk()

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.widgets import Button
import dm3_lib
import numpy
import math
#window_DFH = Tkinter.Tk()
window_DFH.withdraw()

global image, image1
global image_fft_display2
global ax1, ax2
global circle
global mask_fft


def openfile(self):
    global image
    global ax1
    #window_DFH1 = Tkinter.Tk()
    filepath = tkFileDialog.askopenfilename(title="Open an image")
    image = dm3_lib.DM3(filepath)
    ax1.imshow(image.imagedata,cmap="gray")
    print("Bouton cliquee")
    print(image)
    print(image.size[0],image.size[1])
    #window_DFH1.destroy()
    return image

def FFT(self):
    global ax2
    global circle
    global image_fft_display2
    global fig
    global position
    global radius
    #circle.remove()
    image1 = numpy.fft.fft2(image.imagedata)
    #image2 = numpy.real(numpy.fft.ifft2(image1))
    #image3 = numpy.absolute((numpy.fft.ifft2(image1)))
    #print image2
    #print image3
    image_fft_display = numpy.abs(image1) ** 2
    image_fft_display2 = numpy.fft.fftshift(image_fft_display)
    #ax7.imshow(image2, cmap="gray")
    #ax8.imshow(image3, cmap="gray")
    ax2.imshow(numpy.log1p(image_fft_display2),cmap="gray")
    radius = (ax2.get_ylim()[1]-ax2.get_ylim()[0])/10
    position = ((ax2.get_xlim()[0]+ax2.get_xlim()[1])/2,(ax2.get_ylim()[0]+ax2.get_ylim()[1])/2)
    print(radius,position)
    circle = plt.Circle(position, radius, color='b', fill=False)
    ax2.add_artist(circle)
    print("Bouton cliquee")
    return image_fft_display2,position,radius,circle

def change_circle(event):
    global circle
    global position
    global radius
    global ax2
    if event.button == 1 and ax2 == event.inaxes:
        circle.remove()
        print position, radius
        circle = plt.Circle(position, radius, color='b', fill=False)
        ax2.add_artist(circle)
        event.canvas.draw()
        print("Circle drawn")


def change_posrad(event):
    global circle
    global position,radius
    if event.key == 'a' and ax2 == event.inaxes:
        position = (event.xdata, event.ydata)
        print("position=",position)
    if event.key == 'z' and ax2 == event.inaxes:
        radius = math.sqrt((event.xdata-position[0])**2+(event.ydata-position[1])**2)
        print("r=",radius)
    return position,radius



def mask(self):
    global circle
    global position, radius
    global mask_fft
    global image
    global image_fft_display2
    global image_amplitude
    global image_phase
    global image_fft_masked
    print position[0],position[1]
    print radius
    #mask_fft=[[0 for x in range(0,image.size[0])] for x in range(0,image.size[1])]
    mask_fft=numpy.zeros((image.size[0],image.size[1]))
    #print mask_fft
    i=0
    j=0

    for i in range(0,image.size[0]):
        for j in range(0, image.size[1]):
            if ((i-position[1])**2+(j-position[0])**2)<radius**2:
                mask_fft[i][j]=1
            else:
                mask_fft[i][j]=0
    #perform the masking
    #image_fft_masked=numpy.fft.fftshift(numpy.fft.fft2(image.imagedata))
    image_fft_masked=numpy.multiply(mask_fft,numpy.fft.fftshift(numpy.fft.fft2(image.imagedata)))

    #image_fft_display3 = numpy.log1p(numpy.multiply(mask_fft,image_fft_display2))
    #image_fft_display4 = image_fft_display3 / numpy.max(numpy.max(image_fft_display3))
    #print image_fft_display4
    #print image_fft_display4[position[0]][position[1]], image_fft_display3[position[0]+radius/2][position[1]+radius/2]
    ax2.imshow(numpy.log1p(numpy.multiply(mask_fft,image_fft_display2)), cmap="gray")
    image_fft_masked1=numpy.fft.ifftshift(image_fft_masked)
    image_fft_masked2=numpy.fft.ifft2(image_fft_masked1)
    #print image_fft_masked
    #print mask_fft[position[0]][position[1]]
    #print image_fft_masked[position[0]][position[1]],numpy.fft.fftshift(numpy.fft.fft2(image.imagedata))[position[0]][position[1]]
    #print image_fft_masked[position[0]+2*radius][position[1]+2*radius], numpy.fft.fftshift(numpy.fft.fft2(image.imagedata))[position[0]+2*radius][position[1]+2*radius]
    image_amplitude = numpy.absolute(image_fft_masked2)
    ax7.imshow(numpy.log10(image_amplitude), cmap="gray")
    image_phase=numpy.angle(image_fft_masked2)
    ax8.imshow(image_phase, cmap="gray")
    image_phase2 = numpy.unwrap(numpy.angle(image_fft_masked2))
    fig2=plt.figure()
    ax = plt.gca()
    ax.imshow(image_phase, cmap="gray")
    fig2.show()
    #ax2.imshow(numpy.log10(image_fft_masked), cmap="gray")
    #ax2.imshow(image_fft_masked, cmap="gray")
    #print mask_fft
    #print mask_fft[position[0]][position[1]]
    return mask_fft, image_amplitude, image_phase

def closeprogram(event):
    print "Closing pgm"
    plt.close('all')
    window_DFH.destroy()
    window_DFH.quit()

#w = Tkinter.Label(window_DFH, text="Hello, world!")
#w.pack()

# Frame Hologram

#Frame_Hologram = Tkinter.Frame(window_DFH, height=800, width=800, bd=1)
#Frame_Hologram.pack()

# Open file button
#Button_open = Tkinter.Button(Frame_Hologram, text="Open", command=openfile)
#Button_open.pack()

#Initialize 0 mask with the size of the image

mask_fft = []


fig = plt.gcf()
gs = gridspec.GridSpec(4,15)
ax1 = plt.subplot(gs[0:3,0:3])
ax2 = plt.subplot(gs[0:3,4:7])
ax3 = plt.subplot(gs[3,0])
ax4 = plt.subplot(gs[3,1])
ax5 = plt.subplot(gs[3,2])
ax6 = plt.subplot(gs[3,4:7])
ax7 = plt.subplot(gs[0:3,8:11])
ax8 = plt.subplot(gs[0:3,12:15])
radius = 0
position =(0,0)
#circle = plt.Circle(position, radius, color='b', fill=False)

Button1 = Button(ax3,'Open')
Button1.on_clicked(openfile)
Button2 = Button(ax4,'FFT')
Button2.on_clicked(FFT)
Button3 = Button(ax5,'Draw g')
Button3.on_clicked(mask)

Button4 = Button(ax6,'Mask')
Button4.on_clicked(mask)

ax1.axis('on')
ax2.axis('on')
ax6.axis('on')
print(ax2.get_ylim())
#plt.tight_layout()

fig.canvas.mpl_connect('button_press_event', change_circle)
fig.canvas.mpl_connect('key_press_event', change_posrad)
fig.canvas.mpl_connect('close_event', closeprogram)

#window_DFH.protocol("WM_DELETE_WINDOW", closeprogram)

plt.show()
window_DFH.mainloop()
